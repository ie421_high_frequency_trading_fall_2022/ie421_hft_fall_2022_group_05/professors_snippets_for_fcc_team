# professors_snippets_for_fcc_team

## Instructions for using our project

To use our project make sure to download Google Earth Pro on your computer beforehand: https://www.google.com/earth/about/versions/. Next clone our repository:

```
git clone https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2022/ie421_hft_fall_2022_group_05/professors_snippets_for_fcc_team.git
cd professors_snippets_for_fcc_team
vagrant up
```

All the magic happens in `vagrant up` and it takes a long time to run and displays the entire process. Depending on whether or not you have downloaded the FCC databases before, our project will automatically download the most recent data 
from the FCC and the SEC. Once that is setup all you have to do is open up a browser and head to:

```
http://127.0.0.1:8000/cgi-bin/main.py
```

Once you reach this page a file will be automatically downloaded called `main.py`. This is not a Python file. It is a KML file so rename/change its extension to `main.kml`. Afterwards, open the file in Google Earth Pro and visualize our output.
