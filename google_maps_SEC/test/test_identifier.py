import sys
sys.path.append("/vagrant/google_maps_SEC/src/")
from link_identifier import Identifier
import datacenter_constants as c
import numpy as np

import unittest
from unittest.mock import Mock

class IdentifierTestCase(unittest.TestCase):

	def setUp(self):
		self.mock_connection = Mock()
		self.Identifier = Identifier(self.mock_connection)
		

	def test_interpolate_between_two_coordinates(self):
		fwd_azimuth, latitudes, longitudes = self.Identifier.interpolate_between_two_coordinates(c.CH1_LAT, c.CH1_LON, c.NJ2_LAT, c.NJ2_LON, 5)

		self.assertEqual(91.54244247691483, fwd_azimuth)
		self.assertEqual(np.asarray([41.8533031, 41.5804346, 41.3075661, 41.0346976, 40.7618291]).all(), latitudes.all())
		self.assertEqual(np.asarray([-87.6318695,  -84.23070847, -80.82954745, -77.42838643, -74.0272254]).all(), longitudes.all())