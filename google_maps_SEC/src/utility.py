import numpy as np
import math
import constants 
import urllib.parse
import requests

def calculateDistance(p1, p2, p3):
	assert p1.size == p2.size
	assert p2.size == p3.size

	return np.linalg.norm(np.cross(p2-p1, p1-p3))/np.linalg.norm(p2-p1)

 
# Python 3 program for the
# haversine formula
def haversine(lat1, lon1, lat2, lon2):
     
    # distance between latitudes
    # and longitudes
    dLat = (lat2 - lat1) * math.pi / 180.0
    dLon = (lon2 - lon1) * math.pi / 180.0
 
    # convert to radians
    lat1 = (lat1) * math.pi / 180.0
    lat2 = (lat2) * math.pi / 180.0
 
    # apply formulae
    a = (pow(math.sin(dLat / 2), 2) +
         pow(math.sin(dLon / 2), 2) *
             math.cos(lat1) * math.cos(lat2));
    rad = 6371
    c = 2 * math.asin(math.sqrt(a))
    return rad * c

def extract_lat_long_via_address(address_or_zipcode):
    lat, lng = None, None
    api_key = constants.GOOGLE_MAPS_API_KEY
    base_url = "https://maps.googleapis.com/maps/api/geocode/json"
    endpoint = f"{base_url}?address={urllib.parse.quote(address_or_zipcode)}&key={api_key}"
    # see how our endpoint includes our API key? Yes this is yet another reason to restrict the key
    r = requests.get(endpoint, timeout=5)
    lat = None
    lng = None
    bound_northeast_lat = None
    bound_northeast_long = None
    bound_southwest_lat = None
    bound_southwest_long = None
    formatted_address = None

    if r.status_code not in range(200, 299):
        return lat, lng, bound_northeast_lat, bound_northeast_long, bound_southwest_lat, bound_southwest_long, formatted_address
    try:
        '''
        This try block incase any of our inputs are invalid. This is done instead
        of actually writing out handlers for all kinds of responses.
        '''
        results = r.json()['results'][0] # assume first guess is best guess
        lat = results['geometry']['location']['lat']
        lng = results['geometry']['location']['lng']
        formatted_address = results['formatted_address']

        bound_northeast_lat = results['geometry']['bounds']['northeast']['lat']
        bound_northeast_long = results['geometry']['bounds']['northeast']['lng']
        bound_southwest_lat = results['geometry']['bounds']['southwest']['lat']
        bound_southwest_long = results['geometry']['bounds']['southwest']['lng']
    except:
        pass
    return lat, lng, bound_northeast_lat, bound_northeast_long, bound_southwest_lat, bound_southwest_long, formatted_address
