#!/usr/bin/env python
# coding: utf-8

#---------------------------------------------- > Setup

# ---------->   Imports

import random
import numpy as np
import pandas as pd
import networkx as nx
import pymysql.cursors
import geopandas as gpd
import matplotlib.pyplot as plt
from geopy import distance
from mpl_toolkits.basemap import Basemap as Basemap

inputPath = "/vagrant/sql/"
resultPath = "/vagrant/google_maps_SEC/data/networkX_results/millimetre/"

# ---------->   Defines

# this information is hardcoded
dataCenters =  {
                "CMEAurora" : {
                        "idx": 0,
                        "name":"CMEAurora", 
                        "lat": 41.7965685, 
                        "lon": -88.245206,
                        "searchRadius": 0.01}, 
                "ThreeFityCermakChicago" : {
                        "idx": 1,
                        "name":"ThreeFityCermakChicago", 
                        "lat": 41.8533031, 
                        "lon": -87.6318695,
                        "searchRadius": 0.01},
                "ThreeFityCermakNJ" : {
                        "idx": 2,
                        "name":"ThreeFityCermakNJ", 
                        "lat": 40.5842564, 
                        "lon": -74.2443242,
                        "searchRadius": 0.01},
                "NY5Secaucus" : {
                        "idx": 3,
                        "name":"NY5Secaucus", 
                        "lat": 40.7785384, 
                        "lon": -74.0744313,
                        "searchRadius": 0.01},
                "NY2Secaucus" : {
                        "idx": 4,
                        "name":"NY2Secaucus", 
                        "lat": 40.7772145, 
                        "lon": -74.0781314,
                        "searchRadius": 0.01},  
                # "NY4Secaucus" : {"name":"NY4Secaucus", "lat": 40.7772145, "lon": -74.0781314}, 
                "NJ2Weehawken" : {
                        "idx": 5,
                        "name":"NJ2Weehawken", 
                        "lat": 40.7618291, 
                        "lon": -74.0272254,
                        "searchRadius": 0.01},
                "NYSEMahwah" : {
                        "idx": 6,
                        "name":"NYSEMahwah", 
                        "lat": 41.0783617, 
                        "lon": -74.1551081,
                        "searchRadius": 0.01}, 
                "FedWashingtonDC" : {
                        "idx": 7,
                        "name":"FedWashingtonDC", 
                        "lat": 38.9028647, 
                        "lon": -77.0313835,
                        "searchRadius": 0.01}, 
                "TmxToronto" : {
                        "idx": 8,
                        "name":"TmxToronto", 
                        "lat": 43.6486441, 
                        "lon": -79.3840116,
                        "searchRadius": 0.1}
                }
                
dataCenterNames = list(dataCenters.keys())
numberOfDataCenters = len(dataCenterNames)

print("Number Of Data Centers: ", numberOfDataCenters)

# defining a general basemap for plotting graphs overlay and colormap for path depiction
m = Basemap(
        projection='merc',
        epsg='4269',
        llcrnrlon=-90,llcrnrlat=38,urcrnrlon=-70,urcrnrlat=45,
        # lat_1=32,lat_2=45,lon_0=-95,
        resolution="h",
        suppress_ticks=True)
cmap = ['r', 'b', "yellow", 'm', "orange", "purple", "pink", "brown", "gold", "violet", "limegreen", "darkorange", "grey"]

# Establish Connection to SQL Server
connection = pymysql.connect(host='127.0.0.1',  # type: ignore
						port=3306,
						user='root',
						password='vagrant',
						db='python_fcc_db_test',
						charset='utf8mb4',
						cursorclass=pymysql.cursors.DictCursor,
						autocommit=True)
cursor = connection.cursor()


# ---------->   Helper Functions

# defining the node Object
class Node():
    def __init__(self, lat, lon, t_height, r_height, email,  callsign, companyName, distance):
        if email==None:
            self.id = str(round(lat, 6)) + str(round(lon, 6))
        else:
            self.id = str(round(lat, 6)) + str(round(lon, 6)) + "-" + email
        self.companyName = companyName
        # self.distance = distance
        self.lat = round(lat, 6)
        self.lon = round(lon, 6)
        self.t_height = round(t_height, 6)
        self.r_height = round(r_height, 6)
        self.email = email
        self.callSign = callsign

class nxHelper:
    def __init__(self):
        print("iniitiated..")

    # find points that are in radius of the center by distance
    def findPointsClosestTo(self, centerLat, centerLon, radius, G):
        diff = radius #difference in latitide/longitude allowed to deviate from the center point
        nodesNear10kmOfCenter = filter(lambda n: ((abs(G.nodes[n]['data'].lat - centerLat) <= diff) and (abs(G.nodes[n]['data'].lon - centerLon) <= diff)), list(G.nodes()))
        return list(nodesNear10kmOfCenter)

    # find nodes that have the highest degree - limit number of nodes returned by the second argument
    def findNodesWithHighestDegree(self, G, numberOfTopNodes):
        degreeList = sorted(G.degree, key=lambda x: x[1], reverse=True)
        return degreeList[0:numberOfTopNodes]


#---------------------------------------------- > Data

# ---------->   Reading Input Data
print("Reading Input data...")

# Read data from DB using SQL query and filtered links sent by Matt+Jieshu
fd = open(inputPath+"networkX_millimetre_input.sql", 'r')
sql = fd.read()        
cursor.execute(sql)
results = cursor.fetchall()

print(len(results), " records fetched.")


# ---------->   Processing Data
print("Processing data to generate Graph...")

# senderPoints = list of sender node objects, receiverPoints = list of receiver node objects
senderPoints = []
receiverPoints = []
for result in results:
    myNode = Node(result['lat_decimal'], result['lon_decimal'], result['t_ground_elevation'], result['r_ground_elevation'], result['email'], result['call_sign'], result['company_name'], result['distance'])
    senderPoints.append(myNode)

    myNode_r = Node(result['r_lat_decimal'], result['r_lon_decimal'], result['t_ground_elevation'], result['r_ground_elevation'], result['email'], result['call_sign'], result['company_name'], result['distance'])
    receiverPoints.append(myNode_r)

print("Total number of receiver/receiver points in data:", len(senderPoints))

# Graph initialization
G = nx.Graph()
pos={}

# adding nodes to graph and setting their aboslute position using coordinates in map
for node in [*senderPoints, *receiverPoints]:
    node1 = node.id
    if node1 in G:
        continue
    mx1, my1 = m(node.lon, node.lat)
    pos[node1] = (mx1, my1)
    G.add_node(node1, pos=(node.lat, node.lon), data=node)

# add edges
for i in range(min(len(senderPoints), len(receiverPoints))):
    node1 = senderPoints[i].id
    node2 = receiverPoints[i].id

    # no self loops
    if(node1==node2 or (node1 not in G) or (node2 not in G) or (G.has_edge(node1, node2))):
        continue

    # compute length or distance between sender and receiver node
    dist = distance.distance((senderPoints[i].lat, senderPoints[i].lon), (receiverPoints[i].lat, receiverPoints[i].lon)).miles
    G.add_edge(node1, node2, weight=dist)

del senderPoints, receiverPoints
print("Number of nodes in the Graph:", G.number_of_nodes())
print("Number of edges in the Graph:", G.number_of_edges())


#---------------------------------------------- > Plot paths

# ---------->   Whole graph using filtered data
print("Plotting NetworkX Graph - All points...")

# source: https://stackoverflow.com/questions/19915266/drawing-a-graph-with-networkx-on-a-basemap
nx.draw(G, pos, with_labels=False, node_size=0, node_color='blue', edge_color='white')
m.drawcoastlines()
m.drawcountries()
m.drawstates()
m.bluemarble()
plt.show()


# ---------->   Points closest to exchanges

# 2D matrix to store minimum number of links between any 2 data centers
minNumberOfLinksBetweenDataCenters = np.repeat(10000, numberOfDataCenters*numberOfDataCenters).reshape([numberOfDataCenters, numberOfDataCenters])

# 2D matrix to store shortest path length between any 2 data centers
shortestPathLengthBetweenDataCenters = np.repeat(10000.0, numberOfDataCenters*numberOfDataCenters).reshape([numberOfDataCenters, numberOfDataCenters])

# list of lists to store closest nodes (in randomized order) to each data center (order same as dict)
h = nxHelper()
nodesClosestToDataCenters = []
for A in dataCenters:
    closestPoints = h.findPointsClosestTo(dataCenters[A]["lat"], dataCenters[A]["lon"], dataCenters[A]["searchRadius"], G)
    random.shuffle(closestPoints)
    nodesClosestToDataCenters.append(closestPoints)

# 2D matrix to store geographic distance between any 2 data centers
geographicDistanceBetweenDataCenters = []
for A in dataCenters:
    dist = []
    for B in dataCenters:
        dist.append(distance.distance((dataCenters[A]["lat"], dataCenters[A]["lon"]), (dataCenters[B]["lat"], dataCenters[B]["lon"])).miles)
    geographicDistanceBetweenDataCenters.append(dist)

# logs
print("\n{: >45} {: >20}".format(*["", "Geogrphic Distance (miles)"]))
for A, idxA in zip(dataCenterNames, range(numberOfDataCenters)):
    for B, idxB in zip(dataCenterNames[idxA+1:], range(idxA+1,numberOfDataCenters)):
        print("{: >45} {: >20}".format(*[A+" - "+B, round(geographicDistanceBetweenDataCenters[dataCenters[A]["idx"]][dataCenters[B]["idx"]], 4)]))

# logs
print("\n{: >45} {: >20}".format(*["\t", "#Points near datacenter"]))
for A in dataCenters:
    print("{: >45} {: >20}".format(*[A, len(nodesClosestToDataCenters[dataCenters[A]["idx"]])]))

# display all possible source and destination points
print("Plotting NetworkX Graph - Possible source and destination points based on closest distance to datac centers ...")
for A in dataCenters:
    nx.draw_networkx_nodes(G, pos, nodelist=nodesClosestToDataCenters[dataCenters[A]["idx"]], node_size=1, node_color='white')
m.drawcoastlines()
m.drawcountries()
m.drawstates()
m.bluemarble()
plt.show()


#---------------------------------------------- > Actual Algorithm

# ---------->   find the paths from points in all sets to all other sets
print("Running the path finding algorithm...")

pathsEdgeList = []          # edges in the path for each path
pathsNodeList = []          # nodes in the path for each path
pathsNodeList_dict = {}     # nodes in the path (key - from which to which data center)
pathsNumberOfLinks = []     # number of links in each path
pathsLength = []            # length of each path in miles
pathsEmail = []             # primary email corresponding to each path
pathsCompanyName = []       # primary Company name corresponding to each path
numberOfPathsLimit = 200    # between any two points

# iterate over all sets of points near data centers
for A, idxA in zip(dataCenterNames, range(numberOfDataCenters)):
    for B, idxB in zip(dataCenterNames, range(numberOfDataCenters)):

        if idxA >= idxB:
            continue

        print("Searching paths between", A, " and", B, "...")
        id = A + "-" + B
        pathsNodeList_dict[id] = []

        email_set = set()
        countPaths = 0

        # check for all paths from nodes in set A to nodes in set B 
        print("Paths Found: \n", countPaths, end='\r')
        # print("Paths Found: ", countPaths)
        for nA in nodesClosestToDataCenters[idxA]:
            for nB in nodesClosestToDataCenters[idxB]:

                # id of node is stored in 0th index of the list element's tuple
                numberOfPaths = 0
                redundantPaths = 0

                if( nx.has_path(G, nA, nB) ):

                    if nA==nB:
                        continue

                    shortestPath = nx.shortest_path(G, source = nA, target = nB, weight="weight")
                    shortestPathWeight = nx.path_weight(G, shortestPath, weight="weight")
                    shortestPathLength = len(shortestPath)                                                
                    
                    minNumberOfLinksBetweenDataCenters[idxA][idxB] = min(minNumberOfLinksBetweenDataCenters[idxA][idxB], shortestPathLength)
                    shortestPathLengthBetweenDataCenters[idxA][idxB] = min(shortestPathLengthBetweenDataCenters[idxA][idxB], shortestPathWeight)

                    for path in nx.shortest_simple_paths(G, source = nA, target = nB, weight="weight"):

                        pathWeight = nx.path_weight(G, path, weight='weight')

                        # if path length is not within 50 miles of the distance between datacenter A and B
                        if abs(pathWeight - shortestPathLengthBetweenDataCenters[idxA][idxB]) > min(20, geographicDistanceBetweenDataCenters[idxA][idxB]/20):
                            break

                        # assuming (verified experimentally) that each path = 1 email, remove a path if the email already counted for a path 5X b/w nA and nB
                        prevLen = len(email_set)
                        for node in path: 
                            email_set.add(G.nodes[node]['data'].email)
                        if(len(email_set) == prevLen):
                            redundantPaths = redundantPaths + 1
                            if(redundantPaths>5):
                                continue

                        # add path data
                        pathEdges = list(zip(path,path[1:]))
                        pathsEdgeList.append(pathEdges)
                        pathsNodeList.append(path)
                        pathsNodeList_dict[id].append(path)
                        pathsLength.append(pathWeight)

                        numberOfPaths = numberOfPaths + 1
                        countPaths = countPaths + 1
                        print(countPaths, end='\r')

                        # stop if more than LIMIT number of paths found between 2 points
                        if(numberOfPaths > numberOfPathsLimit):
                            numberOfPaths = 0
                            break
        print(countPaths)

# Append one company name and email per path for path identification
print("Associating company name and email with a path...")
for path in pathsNodeList:
    eSet = set()
    for node in path:
        eSet.add(G.nodes[node]['data'].email)
    if len(eSet)==0:
        raise ValueError("A path should have atleast one (including None) associated email")
    elif len(eSet)==1:
        pathsEmail.append(list(eSet)[0])
    elif list(eSet)[0]==None:
        pathsEmail.append(list(eSet)[1])
    else:
        pathsEmail.append(list(eSet)[0])

for path in pathsNodeList:
    eSet = set()
    for node in path:
        eSet.add(G.nodes[node]['data'].companyName)
    if len(eSet)==0:
        raise ValueError("A path should have atleast one (including None) company names associated ")
    elif len(eSet)==1:
        pathsCompanyName.append(list(eSet)[0])
    elif list(eSet)[0]==None:
        pathsCompanyName.append(list(eSet)[1])
    else:
        pathsCompanyName.append(list(eSet)[0])


# ---------->   Summary

print("Number of Paths Found: ", len(pathsLength))
print("{: >50} {: >20} {: >20}".format(*["\t", "Shortest Path (miles)", "Min # Links (hops)"]))
for A, idxA in zip(dataCenterNames, range(numberOfDataCenters)):
    for B, idxB in zip(dataCenterNames, range(numberOfDataCenters)):
        if idxA >= idxB:
            continue
        if shortestPathLengthBetweenDataCenters[idxA][idxB]==10000:
            print("{: >50} {: >20} {: >20}".format(*[A+" - "+B, "None", "None"]))
        else:
            print("{: >50} {: >20} {: >20}".format(*[A+" - "+B, round(shortestPathLengthBetweenDataCenters[idxA][idxB], 4), minNumberOfLinksBetweenDataCenters[idxA][idxB]]))

# draw graph showing all paths
nx.draw(G, pos, with_labels=False, node_size=0, node_color='blue', edge_color='white')
for i in range(len(pathsLength)):
    nx.draw_networkx_edges(G, pos, edgelist=pathsEdgeList[i],edge_color=cmap[i%10], width=0.5, connectionstyle='arc3, rad = 0.3')
m.drawcoastlines()
m.drawcountries()
m.drawstates()
m.bluemarble()
plt.savefig(resultPath+"PrintAllPaths.pdf")
plt.show(block=True)


#---------------------------------------------- > Exports

# just the nodes and antenna info of any path
pathCallSignFile = open(resultPath+"pathCallSign.txt", "w")
for path, idx in zip(pathsNodeList, range(len(pathsNodeList))):
    for node in path:
        pathCallSignFile.write( str(idx) + " " +
                                str(G.nodes[node]['data'].callSign) + " " + 
                                str(G.nodes[node]['data'].lat) + " " +
                                str(G.nodes[node]['data'].lon) + " " +
                                str(G.nodes[node]['data'].email) + "\n"
                            )

# file for generating the kml (Matt will use this)
table1File = open(resultPath+"table1.txt", "w") # all callsigns with their path number and transmitter and receiver info
table2File = open(resultPath+"table2.txt", "w") # mapping each path number to a company name, email and its length information

table1File.write("path_number call_sign transmitter_lat transmitter_lon receiver_lat receiver_lon transmitter_height receiver_height email \n")
for i, idx in zip(pathsEdgeList, range(len(pathsEdgeList))):
    for j in range(len(i)):
        table1File.write(   str(idx) + " " +
                            str(G.nodes[i[j][0]]['data'].callSign) + " " +  
                            str(G.nodes[i[j][0]]['data'].lat) + " " + 
                            str(G.nodes[i[j][0]]['data'].lon) + " " + 
                            str(G.nodes[i[j][1]]['data'].lat) + " " + 
                            str(G.nodes[i[j][1]]['data'].lon) + " " + 
                            str(G.nodes[i[j][0]]['data'].t_height) + " " + 
                            str(G.nodes[i[j][0]]['data'].r_height) + " " + 
                            str(G.nodes[i[j][0]]['data'].email) + "\n"
                        )

table2File.write("path_number email company_name length latency \n")
for i in range(len(pathsEdgeList)):
    table2File.write(   str(i) + " " +  
                        str(pathsEmail[i]) + " " + 
                        str('"'+str(pathsCompanyName[i]) + '"') + " " +
                        str(pathsLength[i]) + " " + 
                        str(186282.39705122/pathsLength[i]) + "\n"
                    )


# list all associated emails and callsigns in the path nodes into a file
emailSet = set()
callsignSet = set()
for path in pathsNodeList:
    for node in path:
        n = G.nodes[node]['data']
        emailSet.add(n.email)
        callsignSet.add(n.callSign)
emailFile = open(resultPath+"emailFile.txt", "w")
callsignFile = open(resultPath+"callsignFile.txt", "w")

for email in emailSet:
    emailFile.write(str(email) + "\n")
for callsign in callsignSet:
    callsignFile.write(callsign + "\n")


# closing files
pathCallSignFile.close()
table1File.close()
table2File.close()
emailFile.close()
callsignFile.close()
