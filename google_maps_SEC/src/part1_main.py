from link_identifier import Identifier
import pymysql
import pymysql.cursors
import datacenter_constants as c
import pandas as pd
import os


if __name__ == "__main__":
	connection = pymysql.connect(host='127.0.0.1',
							port=3306,
							user='root',
							password='vagrant',
							db='python_fcc_db_test',
							charset='utf8mb4',
							cursorclass=pymysql.cursors.DictCursor,
							autocommit=True)

	identifier = Identifier(connection)

	NJ_secaucus_lat_centroid = (c.NY5_LAT + c.NY2_LAT + c.NY4_LAT) / 3
	NJ_secaucus_lon_centroid = (c.NY5_LON + c.NY2_LON + c.NY4_LON) / 3


	CME_to_NJ_params = {"min_long": -89.9520048113,
						 "max_long": -73.4175809831,
						 "min_lat": 38.5592395744,
						 "max_lat": 43.3453774054,
						 "datacenter_start_lat": c.CME_LAT,
						 "datacenter_start_lon": c.CME_LON,
						 "datacenter_end_lat": NJ_secaucus_lat_centroid,
						 "datacenter_end_lon": NJ_secaucus_lon_centroid,
						 "folder_to_save": "part1Results",
						 "file_name": "linksCMEtoNJ",
						 "distance_threshold": 90000} # 90 KM
	CME_to_CH1_params = {"min_long": -88.8002510667,
						 "max_long": -86.710405767,
						 "min_lat": 41.419116889,
						 "max_lat": 42.396925812,
						 "datacenter_start_lat": c.CME_LAT,
						 "datacenter_start_lon": c.CME_LON,
						 "datacenter_end_lat": c.CH1_LAT,
						 "datacenter_end_lon": c.CH1_LAT,
						 "folder_to_save": "part1Results",
						 "file_name": "linksCMEtoCH1",
						 "distance_threshold": 5000} # 5 KM

	CH1_to_NJ_params = {"min_long": -87.7763188688,
						 "max_long": -73.8566411344,
						 "min_lat": 40.4298067871,
						 "max_lat": 42.396925812,
						 "datacenter_start_lat": c.CH1_LAT,
						 "datacenter_start_lon": c.CH1_LON,
						 "datacenter_end_lat": NJ_secaucus_lat_centroid,
						 "datacenter_end_lon": NJ_secaucus_lon_centroid,
						 "folder_to_save": "part1Results",
						 "file_name": "linksCH1toNJ",
						 "distance_threshold": 5000} # 5 KM

	CME_to_TMX_params = {"min_long": -88.8944679069,
						 "max_long": -77.4052251188,
						 "min_lat": 41.4505509465,
						 "max_lat": 44.5274523937,
						 "datacenter_start_lat": c.CME_LAT,
						 "datacenter_start_lon": c.CME_LON,
						 "datacenter_end_lat": c.TMX_LAT,
						 "datacenter_end_lon": c.TMX_LAT,
						 "folder_to_save": "part1Results",
						 "file_name": "linksCMEtoTMX",
						 "distance_threshold": 20000} # 20 KM

	TMX_to_NJ_params = {"min_long": -80.1700951604,
						 "max_long": 40.170060414,
						 "min_lat": -72.9862647819,
						 "max_lat": 44.4608396881,
						 "datacenter_start_lat": c.TMX_LAT,
						 "datacenter_start_lon": c.TMX_LON,
						 "datacenter_end_lat": NJ_secaucus_lat_centroid,
						 "datacenter_end_lon": NJ_secaucus_lon_centroid,
						 "folder_to_save": "part1Results",
						 "file_name": "linksTMXtoNJ",
						 "distance_threshold": 20000} # 20 KM

	CME_to_FED_params = {"min_long": -88.6233384745,
						 "max_long": -76.6029222147,
						 "min_lat": 38.6510715214,
						 "max_lat": 42.1317193272,
						 "datacenter_start_lat": c.CME_LAT,
						 "datacenter_start_lon": c.CME_LON,
						 "datacenter_end_lat": c.FED_LAT,
						 "datacenter_end_lon": c.FED_LON,
						 "folder_to_save": "part1Results",
						 "file_name": "linksCMEtoFed",
						 "distance_threshold": 22000} # 22 KM

	FED_to_NJ_params = {"min_long": -77.5232719729,
						 "max_long": -73.8890656957,
						 "min_lat": 38.5313890131,
						 "max_lat": 41.4441836498,
						 "datacenter_start_lat": c.FED_LAT,
						 "datacenter_start_lon": c.FED_LON,
						 "datacenter_end_lat": NJ_secaucus_lat_centroid,
						 "datacenter_end_lon": NJ_secaucus_lon_centroid,
						 "folder_to_save": "part1Results",
						 "file_name": "linksFedtoNJ",
						 "distance_threshold": 22000} # 22 KM
	params = [CME_to_NJ_params, CME_to_FED_params, FED_to_NJ_params, CME_to_CH1_params, CH1_to_NJ_params, CME_to_TMX_params, TMX_to_NJ_params]
	tables = ["Microwave", "Millimeter"]
	df_list = []
	for path in params:
		print("Path: %s" % path["file_name"])
		for table in tables:
			print("Table: %s" % table)
			is_microwave_query = False
			if table == "Microwave":
				is_microwave_query = True

			df = identifier.find_nearby_antennas(is_microwave_query,
			 path["min_long"], path["max_long"], path["min_lat"],
			  path["max_lat"], path["datacenter_start_lat"],
			   path["datacenter_start_lon"], path["datacenter_end_lat"],
			    path["datacenter_end_lon"], path["distance_threshold"],
				 os.path.join("/vagrant/google_maps_SEC/src/", path["folder_to_save"]), path["file_name"] + table)
		
			df = df.drop('predicted_bd', axis=1)
			df_list.append(df)
	
	final_df = pd.concat(df_list)
	final_df.to_csv(os.path.join("/vagrant/google_maps_SEC/src/", "part1Results/final.csv"))
