# FCC database is in EPSG:4269 and it uses the ellipse to approximate the earth
ELLIPSOID = 'GRS80'

# CME AURORA 
CME_LAT = 41.7965685
CME_LON = -88.245206

# 350 Cermak Chicago IL, CH1 Equinix datacenter
CH1_LAT = 41.8533031
CH1_LON = -87.6318695

# Nasdaq Carteret Datacenter, NY11 Equinix datacenter
NY11_LAT = 40.5842564
NY11_LON = -74.2443242

# NY5 Equinix datacenter, Secaucus
NY5_LAT = 40.77875118999631
NY5_LON = -74.07234924695284

# NY2 Equinix datacenter, Secaucus
NY2_LAT = 40.777366695630484
NY2_LON = -74.07583900594726

# NY4 Equinix datacenter, Secaucus
NY4_LAT = 40.77646983743858
NY4_LON = -74.06981926427464

# NJ2 Equinix datacenter, Weeweehawken
NJ2_LAT = 40.7618291
NJ2_LON = -74.0272254

# NYSE datacenter, Mahwah
NYSE_LAT = 41.0783617
NYSE_LON = 74.1551081

# Fed Building, Washington DC
FED_LAT = 38.9028647
FED_LON = -77.0313835

# TMX Toronto datacenter
TMX_LAT = 43.6486441
TMX_LON = -79.3840116