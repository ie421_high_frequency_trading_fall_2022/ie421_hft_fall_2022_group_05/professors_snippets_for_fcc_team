import pymysql.cursors
import numpy as np
import pyproj
from tqdm import tqdm
import pandas as pd
import datacenter_constants as constants


class Identifier:

	def __init__(self, connection):
		self.connection = connection
		self.cursor = self.connection.cursor()
		self.geodesic = pyproj.Geod(ellps=constants.ELLIPSOID)

	'''
	If is_microwave_query is false then default to milimeter wave 
	'''
	def executeSQLquery(self, is_microwave_query, min_long, max_long, min_lat, max_lat):
		path_table_to_read = "millimetrewave_paths"
		location_table_to_read = "millimetrewave_location"
		if is_microwave_query:
			path_table_to_read = "microwave_paths"
			location_table_to_read = "fcc_location"
		sql = """
				select
					en.email as email,
					en.entity_name as name,
					en.call_sign as callsign,
					st_x(tlo.lat_long_point) as lon_decimal,
					st_y(tlo.lat_long_point) as lat_decimal,
					st_x(rlo.lat_long_point) as r_lon_decimal,
					st_y(rlo.lat_long_point) as r_lat_decimal
					from 
						licensee_entities as en,
						{path_table} as mp,
						{transmitter_location_table} as tlo,
						{receiver_location_table} as rlo
					where 
						en.call_sign = mp.callsign
						and tlo.call_sign = mp.callsign
						and tlo.location_number = mp.transmit_location_number
						and tlo.lat_long_point IS NOT NULL
						and st_x(tlo.lat_long_point) > {min_long}
						and st_x(tlo.lat_long_point) < {max_long}
						and st_y(tlo.lat_long_point) > {min_lat}
						and st_y(tlo.lat_long_point) < {max_lat}
						and rlo.call_sign = mp.callsign
						and rlo.location_number = mp.receiver_location_number
						and rlo.lat_long_point IS NOT NULL
						and LOWER(email) NOT LIKE "%@t-mobile.com"
						and LOWER(email) NOT LIKE "%@verizonwireless.com"
						and LOWER(email) NOT LIKE "%@verizon.com"
						and LOWER(email) NOT LIKE "%@att.com"
						and LOWER(email) NOT LIKE "%@sprint.com" 
						and LOWER(email) NOT LIKE "%@mail.sprint.com" 
						AND LOWER(email) NOT LIKE "%@comcast.net"
						AND LOWER(email) NOT LIKE "%@charter.com"
						AND LOWER(email) NOT LIKE "%@lumen.com"
						AND LOWER(email) NOT LIKE "%@cox.com"
						AND LOWER(email) NOT LIKE "%@cox-internet.com"
						AND LOWER(email) NOT LIKE "%@alticeusa.com"
						AND LOWER(email) NOT LIKE "%@frontier.com"
						AND LOWER(email) NOT LIKE "%@windstream.com"
						AND LOWER(email) NOT LIKE "%@abc.com"
						AND LOWER(email) NOT LIKE "%@cbs.com"
						AND LOWER(email) NOT LIKE "%@nbc.com"
						AND LOWER(email) NOT LIKE "%@fox.com"
						AND LOWER(email) NOT LIKE "%@clearwire.com"
				""".format(path_table=path_table_to_read, # clearwire bought out by T-mobile
							transmitter_location_table=location_table_to_read,
							receiver_location_table=location_table_to_read,
							min_long=min_long, max_long=max_long,
							min_lat=min_lat, max_lat=max_lat)
		
		self.cursor.execute(sql)
		results = self.cursor.fetchall()
		print("SQL Query Executed Successfully")
		return results


	def interpolate_between_two_coordinates(self, start_lat, start_lon, end_lat, end_lon, num_of_points):
		mainline_fwd_azimuth, mainline_back_azimuth, distance = self.geodesic.inv(start_lon, start_lat, end_lon, end_lat)

		# from https://stackoverflow.com/questions/72652599/how-to-interpolate-lat-long-points-route-between-two-lat-long-points 
		latitudes = np.linspace(start_lat, end_lat, num_of_points)  # 100 points
		longitudes = (start_lon - end_lon)/(start_lat - end_lat)*(latitudes - end_lat) + end_lon
		return mainline_fwd_azimuth, latitudes, longitudes

	def find_antennas_near_path(self, results, mainline_fwd_azimuth, latitudes, longitudes, distance_threshold, angle_threshold):
		near_shortest_path = set()
		for latitude, longitude in tqdm(zip(latitudes, longitudes)):
			for result in results:
				_, _, haversine_distance = self.geodesic.inv(longitude, latitude, result['lon_decimal'], result['lat_decimal'])
				if haversine_distance < distance_threshold:
					fwd_azimuth, _, path_length = self.geodesic.inv(result['lon_decimal'], result['lat_decimal'], result['r_lon_decimal'], result['r_lat_decimal'])
					if abs(mainline_fwd_azimuth - fwd_azimuth) <= angle_threshold and path_length < 150000: # links are usually 100 meters apart (Using 150 for margin of error)
						near_shortest_path.add(tuple([result['callsign'], result['email'], result['lat_decimal'], result['lon_decimal'], None])) 
						results.remove(result)

		near_shortest_path_list = list(near_shortest_path)
		to_list = []
		for entry in near_shortest_path_list:
			to_list.append(list(entry))

		df = pd.DataFrame(to_list, columns = ["callsign", "email", "lat", "lon", "predicted_bd"])
		return df
			

	def find_nearby_antennas(self, is_microwave_query, min_long, max_long, min_lat, max_lat,
							datacenter_start_lat, datacenter_start_lon, datacenter_end_lat, datacenter_end_lon,
							distance_threshold, folder_to_save, file_name):
		angle_threshold = 45 # in degrees
		interpolation_number_of_points = 100

		results = self.executeSQLquery(is_microwave_query, min_long, max_long, min_lat, max_lat)
		fwd_azimuth, latitudes, longitudes = self.interpolate_between_two_coordinates(datacenter_start_lat,
												datacenter_start_lon, datacenter_end_lat, datacenter_end_lon, interpolation_number_of_points)

		df = self.find_antennas_near_path(results, fwd_azimuth, latitudes, longitudes, distance_threshold, angle_threshold)

		print("{fld}/{fname}.csv".format(fld=folder_to_save, fname= file_name))
		df.to_csv("{fld}/{fname}.csv".format(fld=folder_to_save, fname= file_name))


		return df
