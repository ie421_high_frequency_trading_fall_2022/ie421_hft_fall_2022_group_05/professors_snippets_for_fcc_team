import pymysql.cursors
import utility

class FCC_Populator():

		def __init__(self, cursor):
			self.MAIN_SQL_QUERY = "SELECT DISTINCT street_address, city, state, zip_code from entities"
			self.db_connection_cursor = cursor
			self.TABLE_CHECK = """drop table if exists FCCPoints"""
			self.CREATE_TABLE = """
								CREATE TABLE FCCPoints (
								street_address varchar(60) null,
								city varchar(40) null,
      							state varchar(20) null,
      							zip_code varchar(9) null,
								lat REAL null,
								lon REAL null,
								bd_ne_lat REAL null,
								bd_ne_lon REAL null,
								bd_sw_lat REAL null,
								bd_sw_lon REAL null,
								formatted_address varchar(250) null
								)
								"""

		def create_point_table(self):
			self.db_connection_cursor.execute(self.TABLE_CHECK)
			self.db_connection_cursor.execute(self.CREATE_TABLE)

		def main_extraction(self):
			self.db_connection_cursor.execute(self.MAIN_SQL_QUERY)
			fcc_table_result = self.db_connection_cursor.fetchall()
			self.query_googlemaps_using_table(fcc_table_result)

		def query_googlemaps_using_table(self, fcc_table):
			count = 0
			for row in fcc_table:
				address = ' '.join(filter(None, (row['street_address'], row['city'], row['state'], row['zip_code'])))

				lat, long, bd_ne_lat, bd_ne_lon, bd_sw_lat, bd_sw_lon, formatted_address = utility.extract_lat_long_via_address(address)

				# making it sql readable
				lat = str(lat or "null")
				long = str(long or "null")
				bd_ne_lat = str(bd_ne_lat or "null")
				bd_ne_lon = str(bd_ne_lon or "null")
				bd_sw_lat = str(bd_sw_lat or "null")
				bd_sw_lon = str(bd_ne_lon or "null")
				str_adr = str(row['street_address'] or "null").replace('"', "'").replace("'", '\'\'\'')
				city = str(row['city'] or "null").replace('"', '\'\"\'').replace("'", "\'\'\'")
				state = str(row['state'] or "null").replace('"', '\'\"\'').replace("'", "\'\'\'")
				zip = str(row['zip_code'] or "null").replace('"', '\'\"\'').replace("'", "\'\'\'")
				fa = str(formatted_address or "null").replace('"', '"').replace("'", '\'\'\'')

				update_query = "INSERT INTO FCCPoints VALUES (\"{str_adr}\", \"{city}\", \"{state}\", \"{zip}\", {lat}, {lon}, {bnla}, {bnlo}, {bsla}, {bslo}, \"{fa}\")".format(str_adr=str_adr, city=city, state=state, zip=zip, lat=lat, lon=long, bnla=bd_ne_lat, bnlo=bd_ne_lon, bsla=bd_sw_lat, bslo=bd_sw_lon, fa=fa)

				self.db_connection_cursor.execute(update_query)
				count += 1
				if count % 500 == 0:
					print("Count: ", count)


if __name__ == "__main__":

	connection = pymysql.connect(host='127.0.0.1',
				port=3306,
				user='root',
				password='vagrant',
				db='python_fcc_db_test',
				charset='utf8mb4',
				cursorclass=pymysql.cursors.DictCursor,
				autocommit=True)

	populator = FCC_Populator(connection.cursor())
	populator.create_point_table()
	populator.main_extraction()


		

