import pymysql.cursors
import requests
import urllib.parse
import constants
import utility



class SEC_Populator():

	def __init__(self, db_connection_cursor):
		self.MAIN_SQL_QUERY = "select cik_number as id, company_name as name, address1 as address, city as city, state_code as state, zip_code as zip_code FROM bds"
		self.PROBLEMATIC_ADDRESSES_QUERY = "select cik_number as id FROM bds WHERE address1 LIKE '%PO BOX%' OR address1 LIKE '%P.O.%' or address1 LIKE '%P%O%box%' "
		self.CREATE_COLUMNS = """ALTER TABLE bds 
								ADD COLUMN lat REAL AFTER zip_code,
								ADD COLUMN lon REAL AFTER lat,
								ADD COLUMN bd_ne_lat REAL AFTER lon,
								ADD COLUMN bd_ne_lon REAL AFTER bd_ne_lat,
								ADD COLUMN bd_sw_lat REAL AFTER bd_ne_lon,
								ADD COLUMN bd_sw_lon REAL AFTER bd_sw_lat"""
		self.db_connection_cursor = db_connection_cursor

	def create_columns(self):
		self.db_connection_cursor.execute(self.CREATE_COLUMNS)


	def main_extraction(self):
		self.db_connection_cursor.execute(self.MAIN_SQL_QUERY)
		sec_table_result = self.db_connection_cursor.fetchall()

		self.db_connection_cursor.execute(self.PROBLEMATIC_ADDRESSES_QUERY)
		problematic_addresses = self.db_connection_cursor.fetchall()

		self.query_googlemaps_using_table(sec_table_result, problematic_addresses)

	def query_googlemaps_using_table(self, sec_table, problematic_addresses):
		addresses_set = set([dict_resp['id'] for dict_resp in problematic_addresses])
		count = 0
		for row in sec_table:
			if row['id'] in addresses_set: # P.O. Boxes mess up google maps api call
				address = ' '.join(filter(None, (row['name'], row['city'], row['state'], row['zip_code'])))
			else:
				address = ' '.join(filter(None, (row['name'], row['address'], row['city'], row['state'], row['zip_code'])))
			
			lat, long, bd_ne_lat, bd_ne_lon, bd_sw_lat, bd_sw_lon, address = utility.extract_lat_long_via_address(address)

			# making it sql readable
			lat = str(lat or "null")
			long = str(long or "null")
			bd_ne_lat = str(bd_ne_lat or "null")
			bd_ne_lon = str(bd_ne_lon or "null")
			bd_sw_lat = str(bd_sw_lat or "null")
			bd_sw_lon = str(bd_ne_lon or "null")


			update_query = "UPDATE bds SET lat = {lat}, lon = {lon}, bd_ne_lat = {bnla}, bd_ne_lon = {bnlo}, bd_sw_lat = {bsla}, bd_sw_lon = {bslo} WHERE cik_number = {id}".format(lat=lat, lon=long, bnla=bd_ne_lat, bnlo=bd_ne_lon, bsla=bd_sw_lat, bslo=bd_sw_lon, id=row['id'])
			self.db_connection_cursor.execute(update_query)
			count += 1

			if count % 100 == 0:
				print("Count: ", count)
	
	
if __name__ == "__main__":

	connection = pymysql.connect(host='127.0.0.1',
						port=3306,
						user='root',
						password='vagrant',
						db='python_fcc_db_test',
						charset='utf8mb4',
						cursorclass=pymysql.cursors.DictCursor,
						autocommit=True)

	populator = SEC_Populator(connection.cursor())
	populator.create_columns()
	populator.main_extraction()