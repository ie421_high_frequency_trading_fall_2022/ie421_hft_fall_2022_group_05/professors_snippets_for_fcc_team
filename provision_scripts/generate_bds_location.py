import pymysql.cursors
import simplekml
import requests

def gmapQuery(address):
    api_key = None
    # Set up your Geocoding url
    geocode_url = "https://maps.googleapis.com/maps/api/geocode/json?address={}".format(address)
    if api_key is not None:
        geocode_url = geocode_url + "&key={}".format(api_key)
    # Ping google for the results:
    results = requests.get(geocode_url)
    # Results will be in JSON format - convert to dict using requests functionality
    results = results.json()
    return results
def getlatLong(address, address1, address2):
    results = gmapQuery(address)
    if len(results['results']) == 0:
        results = gmapQuery(address1)
    if len(results['results']) == 0:
        results = gmapQuery(address2)
    if len(results['results']) == 0:
        print(f"""Cannot find {address}""")
        return (None, None, None)
    answer = results['results'][0]
    lat = answer.get('geometry').get('location').get('lat')
    lon = answer.get('geometry').get('location').get('lng')
    formatted_address = answer.get('formatted_address')
    return (lat, lon, formatted_address)

def connect():
    connection = pymysql.connect(host='127.0.0.1',
                                port=3306,
                                user='root',
                                password='vagrant',
                                db='python_fcc_db_test',
                                charset='utf8mb4',
                                cursorclass=pymysql.cursors.DictCursor)
    return connection

def createbdsLocationEntityTable(connection, cursor):
    sql = """DROP TABLE IF EXISTS bds_location"""
    cursor.execute(sql)
    sql = """
       CREATE TABLE bds_location 
       (cik_number	varchar(10) null,
        company_name varchar(60) null,
        latitude float null,
        longitude float null,
        formatted_address varchar(250) null)
    """
    cursor.execute(sql)
    connection.commit()

def loadbdsLocationEntityTable(connection, cursor):
    createbdsLocationEntityTable(connection, cursor)
    secQuery = "Select * from bds"
    cursor.execute(secQuery)
    bdsInfo = cursor.fetchall()
    for row in bdsInfo:
        cik_number = row["cik_number"]
        company_name = row["company_name"]
        reporting_file_number = row["reporting_file_number"]
        address1 = str(row["address1"] or "")
        address2 = str(row["address2"] or "")
        city = str(row["city"] or "")
        state_code = str(row["state_code"] or "")
        zip_code = row["zip_code"]
        address = f"""{address1}, {address2}, {city}, {state_code}, {zip_code}"""
        address = address.replace("#", "%23")
        address1 = f"""{address1}, {city}, {state_code}, {zip_code}"""
        address1 = address1.replace("#", "%23")
        address2 = f"""{address2}, {city}, {state_code}, {zip_code}"""
        address2 = address2.replace("#", "%23")
        lat, lon, formatted_address = getlatLong(address, address1, address2)
        if not lat or not lon or not formatted_address:
            insertQuery = f"""INSERT INTO bds_location VALUES ({cik_number}, "{company_name}", null, null, null)"""
        else:
            insertQuery = f"""INSERT INTO bds_location VALUES ({cik_number}, "{company_name}", {lat}, {lon}, "{formatted_address}")"""
        res = cursor.execute(insertQuery)
    connection.commit()

def main():
    try: 
        connection = connect()
        cursor = connection.cursor()
        loadbdsLocationEntityTable(connection, cursor)
    except Exception as error:
        print("Error:")
        print(error)
    finally:
        connection.close()
    
main()
