#!/bin/bash

# i hate windows. Me too!
sed -i 's/\r$//' /vagrant/provision_scripts/generate_data.sh

DATA_FOLDER="/vagrant/data"

sudo dnf install unzip 

echo "Generating data"

echo "Generating data folder if it does not exist"

mkdir -p $DATA_FOLDER

mkdir -p $DATA_FOLDER/fcc_l_micro

echo "Target download dir is $DATA_FOLDER"

echo "Checking if data needs to be updated"
if [ -d "$DATA_FOLDER/fcc_l_micro" ]
then
	if [ "$(ls -A $DATA_FOLDER/fcc_l_micro)" ]; then
     	echo "No need to update. Folder is nonempty."
	else
		echo "Directory empty, downloading files..."
		python3 /vagrant/provision_scripts/download_from_fcc.py $DATA_FOLDER
		echo "Extracting files"
		unzip $DATA_FOLDER/l_micro.zip -d $DATA_FOLDER/fcc_l_micro
		echo "Finished Extraction."
	fi
else
	echo "Directory $DIR not found."
fi

# check if the data was successfully downloaded from FCC - check if folder is empty
if [ "$(ls -A $DATA_FOLDER/fcc_l_micro)" ]; then 
	echo "Micro data extracted."
else
	echo "Micro Data extraction unsucessful."
	exit 0
fi

echo "Working on Downloading SEC database right now..."
python3 /vagrant/provision_scripts/download_from_sec.py $DATA_FOLDER

# checking if the bd.txt has a non-zero size
if [ -s $DATA_FOLDER/bd.txt ]; then
        echo "SEC data extracted."
else
        echo "SEC data extraction unsuccessful."
		exit 0
fi

echo "Working on Downloading Milimetre Wave Data from Micronetcom..."
python3 /vagrant/provision_scripts/download_mm_wave.py $DATA_FOLDER

echo "Extracting milimetre wave files"
unzip -o $DATA_FOLDER/l_7090.zip  -d $DATA_FOLDER/l_7090

# check if the data was successfully downloaded from Micronetcom - check if folder is empty
if [ "$(ls -A $DATA_FOLDER/l_7090)" ]; then 
	echo "Milimetre Wave data extracted."
else
	echo "Milimetre Wave data extraction unsuccessful."
	exit 0
fi

echo "Ensuring data folder is readable by all users (including msyql user for doing LOAD DATA INFILE)"
chmod -R 777 $DATA_FOLDER

