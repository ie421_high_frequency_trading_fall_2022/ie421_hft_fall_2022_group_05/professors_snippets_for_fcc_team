#install other packages needed for db
echo "Installing python and its libraries..."
sudo dnf install -y python3-pip
# sudo dnf install python3-devel -y
# sudo yum install geos-devel -y
# python3 -m pip install cartopy
python3 -m pip install PyMySQL
python3 -m pip install simplekml
python3 -m pip install requests
python3 -m pip install ipykernel
python3 -m pip install matplotlib
python3 -m pip install pandas
python3 -m pip install geopandas
python3 -m pip install geopy
# python3 -m pip install plotly
python3 -m pip install nbformat
# python3 -m pip install geoplot
# python3 -m pip install rtree
python3 -m pip install pyproj
python3 -m pip install geos
python3 -m pip install numpy
python3 -m pip install networkx
python3 -m pip install basemap
python3 -m pip install basemap-data
python3 -m pip install basemap-data-hires
python3 -m pip install geopandas
python3 -m pip install tqdm