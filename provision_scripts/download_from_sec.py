from ast import pattern
import sys, requests, re, os


def download_sec(argv):
	assert len(argv) == 1 # the argument should only be the path to the data folder

	print("Downloading registered SEC Brokers and Dealers...")
	# TODO: Theres a pattern to automate this but its too much work...
	most_recent_br_url = r'https://www.sec.gov/files/data/broker-dealers/company-information-about-active-broker-dealers/bd100122.txt'
	r = requests.get(most_recent_br_url)
	print("Got content. Writing to bd.txt file")
	with open(argv[0] + "/bd.txt", "w+") as f:
		f.write(r.text)
	print("Finished SEC database")

if __name__ == "__main__":
	download_sec(sys.argv[1:])