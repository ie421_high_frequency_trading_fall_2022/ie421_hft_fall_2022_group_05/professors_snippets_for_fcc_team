# check if https (Apache) is installed or not
# commandLen=`expr "$(whereis httpd)" : '.*'`
# packageLen=6
# if [ "$commandLen" -gt "$packageLen" ]; then 
# 	echo "Apache installed."
# else
# 	echo "Apache installation not found."
# 	exit 0
# fi

# check if mySQL is installed or not
commandLen=`expr "$(whereis mysql)" : '.*'`
packageLen=6
if [ "$commandLen" -gt "$packageLen" ]; then 
	echo "mySQL installed."
else
	echo "mySQL installation not found."
	exit 0
fi

# check if Python is installed or not
commandLen=`expr "$(whereis python3)" : '.*'`
packageLen=8
if [ "$commandLen" -gt "$packageLen" ]; then 
	echo "Python 3 installed."
else
	echo "Python 3 installation not found."
	exit 0
fi

# check if the mysql service is up and running - https://gist.github.com/mheadd/5571023
UP=$(pgrep mysql | wc -l);
if [ "$UP" -ne 1 ];
then
    echo "MySQL service is not running.";
    exit 0
else
    echo "MySQL sevice up and running.";
fi

# check if the apache service is up and running - https://unix.stackexchange.com/questions/633115/what-is-the-best-and-the-efficient-way-of-tracking-whether-apache-web-server-is
if ps -aux | grep 'http[d]' ; then
    echo "Apache server up and running."
else 
    echo "Apache server not running."
    exit 0
fi

echo "Regenerating database"
mysql -u root -pvagrant < /vagrant/sql/create_db.sql
echo "Finished generating database"

echo "Start FCC data entry"
echo "Regenerating FCC Entities Table"
mysql -u root -pvagrant < /vagrant/sql/microwave/create_entities_table.sql
echo "Finished generating FCC Entities Table"

echo "Regenerating FCC location Table"
mysql -u root -pvagrant < /vagrant/sql/microwave/create_fcc_location_table.sql
echo "Finished generating FCC location Table"

echo "Regenerating FCC microwave_paths Table"
mysql -u root -pvagrant < /vagrant/sql/microwave/create_microwave_paths_table.sql
echo "Finished generating FCC microwave_paths Table"

echo "Regenerating FCC leased_microwave_paths Table"
mysql -u root -pvagrant < /vagrant/sql/microwave/create_leased_microwave_paths_table.sql
echo "Finished generating FCC leased_microwave_paths Table"

echo "Regenerating FCC microwave_license_header Table"
mysql -u root -pvagrant < /vagrant/sql/microwave/create_microwave_license_header_table.sql
echo "Finished generating FCC microwave_license_header Table"
echo "Finished FCC Microwave Data Entry"

echo "Start Millimetrewave Data Entry"
echo "Regenerating millimetrewave_location Table"
mysql -u root -pvagrant < /vagrant/sql/millimetre/create_millimetrewave_location_table.sql
echo "Finished generating millimetrewave_location Table"

echo "Regenerating millimetrewave_paths Table"
mysql -u root -pvagrant < /vagrant/sql/millimetre/create_millimetrewave_paths_table.sql
echo "Finished generating millimetrewave_paths Table"
echo "Finished Millimetrewave Data Entry"

echo "Regenerating entities of interest table"
mysql -u root -pvagrant < /vagrant/sql/create_entities_of_interest_table.sql
echo "Finished generating entities of interest table"

echo "Regenerating SEC database"
mysql -u root -pvagrant < /vagrant/sql/create_sec_tables.sql
echo "Finished generating SEC data"

echo "Regenerating Antenna data"
mysql -u root -pvagrant < /vagrant/sql/create_antenna_tables.sql
echo "Finished generating Antenna Data"
    
echo "Filtering Entity data by licensee entity type"
mysql -u root -pvagrant < /vagrant/sql/create_licensee_entities_table.sql
echo "Finished filtering entity Data"

echo "Regenerating OET shortwave data"
mysql -u root -pvagrant < /vagrant/sql/shortwave/create_oet_shortwave_table.sql
echo "Finished generating OET shortwave Data"

echo "Regenerating IL bounding box tables"
mysql -u root -pvagrant < /vagrant/sql/shortwave/create_il_bounding_box_tables.sql
echo "Finished generating IL bounding box tables"

echo "Regenerating NJ bounding box tables"
mysql -u root -pvagrant < /vagrant/sql/shortwave/create_nj_bounding_box_tables.sql
echo "Finished generating NJ bounding box tables"

echo "Regenerating shortwave microwave antenna pairs table with illinois"
mysql -u root -pvagrant < /vagrant/sql/shortwave/create_shortwave_antenna_pairs_table_for_il.sql
echo "Finished shortwave microwave antenna pairs table with illinois"

echo "Adding shortwave microwave antenna pairs table with new jersey"
mysql -u root -pvagrant < /vagrant/sql/shortwave/create_shortwave_antenna_pairs_table_for_nj.sql
echo "Finished adding shortwave microwave antenna pairs table with new jersey"

echo "Load Entity Company Matching Table -Notice: This only load the cached data, To get the most up to date data, run ./generate_entity_company.sh"
mysql -u root -pvagrant < /vagrant/sql/create_entity_company.sql
echo "Finished loading entity company"

# echo "Regenerating Entity Company Matching Table"
# ./generate_entity_company.sh
# echo "Finished generating entity company"

sed -i 's/\r$//' /vagrant/provision_scripts/automated_link_identification_part1.sh
sed -i 's/\r$//' /vagrant/provision_scripts/automated_link_identification_part2.sh
sed -i 's/\r$//' /vagrant/provision_scripts/automated_link_identification_part3.sh



