import os, sys, time, requests, zipfile, io


def download_fcc(argv):
	assert len(argv) == 1 # the argument should only be the path to the data folder

	print("Refreshing Data...")

	microwave_3650_3700MHZ_url = r'https://data.fcc.gov/download/pub/uls/complete/l_micro.zip'

	r = requests.get(microwave_3650_3700MHZ_url)
	print("Got content. Writing to zip file")
	with open(argv[0] + "/l_micro.zip", "wb+") as f:
		f.write(r.content)
	print("Finished updating database")

if __name__ == "__main__":
	download_fcc(sys.argv[1:])