mkdir /vagrant/google_maps_SEC/data/networkX_results

echo "Running the code for path finding algorithm in microwave..."
mkdir /vagrant/google_maps_SEC/data/networkX_results/microwave
python3 /vagrant/google_maps_SEC/src/networkX_experiments_microwave.py

echo "Running the code for path finding algorithm in millimetre..."
mkdir /vagrant/google_maps_SEC/data/networkX_results/millimetre
python3 /vagrant/google_maps_SEC/src/networkX_experiments_millimetre.py

echo "Loading data into tables"
mysql -u root -pvagrant < /vagrant/sql/create_final_tables.sql
echo "Finished loading data into tables microwave_table1, microwave_table2, milimetrewave_table1, milimetrewave_table2"