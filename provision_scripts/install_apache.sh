#!/bin/bash

echo "Installing Apache Webserver and associated tools"
#TODO: can probalby cut this down to httpd and maybe a few required plugins
sudo dnf -y install httpd
# sudo dnf -y group install "Web Server"

#sudo firewall-cmd --add-service=http --add-service=https --permanent
#sudo firewall-cmd --reload

sudo cp /vagrant/provision_files/httpd.conf /etc/httpd/conf/httpd.conf
sudo chmod 644 /etc/httpd/conf/httpd.conf

sed -i 's/\r$//' /vagrant/google_earth_server/cgi-bin/main.py
sudo ln -s /vagrant/google_earth_server/cgi-bin/test_html2.py /var/www/cgi-bin/main.py

sudo systemctl start httpd.service
sudo systemctl enable httpd.service
echo "Finish installing apache"

