# Performing Part 1
mkdir /vagrant/google_maps_SEC/src/part1Results

echo "Performing initial filtering of data by paths between Chicago and New Jersey..."
python3 /vagrant/google_maps_SEC/src/part1_main.py
echo "Finished generating final.csv. Loading it into MySQL table named part1table"
mysql -u root -pvagrant < /vagrant/sql/part1/loadIntoTable.sql
echo "Finished loading into part1table"