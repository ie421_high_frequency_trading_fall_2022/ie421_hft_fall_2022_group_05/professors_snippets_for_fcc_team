import pymysql.cursors
import simplekml
import threading

### Find top ten nearest annetana (from Lo.DAT) near the broker-dealer address (from SEC)
### Mysql accept at most 100 connection
### Even with treading, this function is still very slow
### Probably can be used for sanity check

def connect():
    connection = pymysql.connect(host='127.0.0.1',
                                     port=3306,
                                     user='root',
                                     password='vagrant',
                                     db='python_fcc_db_test',
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)
    return connection

class queryThread(threading.Thread):
    def __init__(self, company_latitude, company_longitude):
        threading.Thread.__init__(self)
        self.company_latitude = company_latitude
        self.company_longitude = company_longitude

    def run(self):
        connection = connect()
        cursor = connection.cursor()
        query = f"""
            Select  
                call_sign, latitude, longitude,
                ( 6371 * acos( cos( radians({self.company_latitude}) ) * 
                cos( radians( latitude ) ) 
                * cos( radians( longitude ) - radians({self.company_longitude}) ) 
                + sin( radians({self.company_latitude}) ) * sin(radians( latitude)) ) ) AS distance 
            From location as loc, microwave_paths as mp
            WHERE 
                loc.call_sign = mp.callsign 
                and loc.location_number = mp.transmit_location_number
                and latitude is not null
                and longitude is not null
                and latitude > -90
                and latitude < 90
                and longitude > -180
                and longitude < 180
            Order by distance
            Limit 10
        """
        cursor.execute(query)
        fields = cursor.fetchall()

        call_signs = []
        latitudes = []
        longitudes = []
        for field in fields:
            call_signs.append(field["call_sign"])
            latitudes.append(field["latitude"])
            longitudes.append(field["longitude"])
        insertQuery = f'''
        INSERT INTO bds_nearest_towers  
        VALUES (cik_number, Point({self.company_longitude}, {self.company_latitude}), 
        Point({longitudes[0]}, {latitudes[0]}), '{call_signs[0]}',
        Point({longitudes[1]}, {latitudes[1]}), '{call_signs[1]}',
        Point({longitudes[2]}, {latitudes[2]}), '{call_signs[2]}',
        Point({longitudes[3]}, {latitudes[3]}), '{call_signs[3]}',
        Point({longitudes[4]}, {latitudes[4]}), '{call_signs[4]}',
        Point({longitudes[5]}, {latitudes[5]}), '{call_signs[5]}',
        Point({longitudes[6]}, {latitudes[6]}), '{call_signs[6]}',
        Point({longitudes[7]}, {latitudes[7]}), '{call_signs[7]}',
        Point({longitudes[8]}, {latitudes[8]}), '{call_signs[8]}',
        Point({longitudes[9]}, {latitudes[9]}), '{call_signs[9]}'
        )
        '''
        # print(insertQuery)
        res = cursor.execute(insertQuery)
        connection.commit()
        connection.close()

        # print(res)
MAX_THREAD = 90
def main():
    try:
        connection = connect()
        cursor = connection.cursor()
        sql = """
            select distinct latitude, longitude, cik_number from bds_location
        """
        cursor.execute(sql)
        sec_points = cursor.fetchall()
        number_of_query = len(sec_points)
        query_index = 0
        threads = []
        while query_index < number_of_query:
            while len(threads) < MAX_THREAD and query_index < number_of_query:
                company_latitude = sec_points[query_index]['latitude']
                company_longitude = sec_points[query_index]['longitude']
                new_thread = queryThread(company_latitude, company_longitude)
                threads.append(new_thread)
                query_index += 1
            for thread in threads:
                thread.start()
            for thread in threads:
                thread.join()
            threads.clear()
            print("finish one batch!")

    except Exception as error:
        print("Error")
        print(error)
    finally:
        connection.close()
        
main()
