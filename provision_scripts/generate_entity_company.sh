echo "Warning: Please add api_key in find_entity_company_mailing.py and generate_bds_location.py"
echo "And make sure you have enough credit in your Google cloud account!"

echo "Generating SEC data with location"
python3 /vagrant/provision_scripts/generate_bds_location.py
echo "Finish generating SEC data with location"
echo "Working on generate entity-company mapping..."
python3 /vagrant/provision_scripts/find_entity_company_mailing.py
echo "Finish generating entity company mapping"
