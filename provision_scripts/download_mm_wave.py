import sys, requests

def download_mm(argv):
	assert len(argv) == 1 # the argument should only be the path to the data folder

	print("Refreshing MM Data...")

	microwave_mm_url = r'https://www.micronetcom.com/LinkRegistration/l_7090.zip'

	r = requests.get(microwave_mm_url)
	print("Got content. Writing to zip file")
	with open(argv[0] + "/l_7090.zip", "wb+") as f:
		f.write(r.content)
	print("Finished updating MM Wave database")

if __name__ == "__main__":
	download_mm(sys.argv[1:])