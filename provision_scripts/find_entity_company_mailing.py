from datetime import datetime
import pymysql.cursors
import simplekml
import requests

### Step 1: Group entity by mailing address (street_address, city, state, zip_code) 
###          and filter addresses that has less than 10 rows 
###          (The purpose of this step to reduce the number of query to google map and exclude outlier, changable or skippable)
### Step 2: Use google map api to query the lat & lon of these addresses and write the result to the table EntityMailingAddress
### Step 3: Map entity mailing address to company by distance (CompanyMailingAddress)
### Step 3: Join licensee_entities with CompanyMailingAddress and create the final table EntityCompany

def gmapQuery(address):
    api_key = None
    address = address.replace(" ", '%20')
    address = address.replace("&", "%26")
    address = address.replace("#", "%23")
    # Set up your Geocoding url
    geocode_url = "https://maps.googleapis.com/maps/api/geocode/json?address={}".format(address)
    if api_key is not None:
        geocode_url = geocode_url + "&key={}".format(api_key)
    # Ping google for the reuslts:
    results = requests.get(geocode_url)
    # Results will be in JSON format - convert to dict using requests functionality
    results = results.json()
    return results

def getlatLong(address):
    results = gmapQuery(address)
    if len(results['results']) == 0:
        print(f"""Cannot find {address}""")
        return (None, None, None)
    answer = results['results'][0]
    lat = answer.get('geometry').get('location').get('lat')
    lon = answer.get('geometry').get('location').get('lng')
    formatted_address = answer.get('formatted_address') or "null"
    return (lat, lon, formatted_address)

def connect():
    connection = pymysql.connect(host='127.0.0.1',
                                port=3306,
                                user='root',
                                password='vagrant',
                                db='python_fcc_db_test',
                                charset='utf8mb4',
                                cursorclass=pymysql.cursors.DictCursor)
    return connection

def createEntityTable(connection, cursor):
    sql = """DROP TABLE IF EXISTS EntityMailingAddress"""
    cursor.execute(sql)
    sql = """
       CREATE TABLE EntityMailingAddress 
       (entity_count int, street_address varchar(60) null, city varchar(40) null, 
       state varchar(20) null, zip_code varchar(9) null, lat REAL null, lon REAL null, formatted_address varchar(250) null)
    """
    cursor.execute(sql)
    connection.commit()

def loadEntityTable(connection, cursor, mailingAddress):
    createEntityTable(connection, cursor)
    print("Create Table EntityMailingAddress")
    for row in mailingAddress:
        count = row["count"]
        street = str(row["street_address"] or "")
        city = str(row["city"] or "")
        state = str(row["state"] or "")
        zip_code = str(row["zip_code"] or "")
        if len(street) < 3:
            continue
        address = f"""{street}, {city}, {state}, {zip_code}"""
        lat, lon, formatted_address = getlatLong(address)
        if lat and lon and formatted_address:
            insertQuery = f"""INSERT INTO EntityMailingAddress VALUES ({count}, "{street.replace('"', "'")}", "{city}", "{state}", {zip_code}, {lat}, {lon}, "{formatted_address}")"""
            # print(insertQuery)
            try:
                res = cursor.execute(insertQuery)
            except Exception as error:
                print (error)
                continue
        # res = cursor.execute(insertQuery)
    connection.commit()

# Match mailing address with company by geo distance
def createCompanyMailingTable(connection, cursor):
    sql = """DROP TABLE IF EXISTS CompanyMailingAddress"""
    cursor.execute(sql)
    sql = """
       CREATE TABLE CompanyMailingAddress (
        street_address varchar(60) null, 
        city varchar(40) null, 
        state varchar(20) null, 
        zip_code varchar(9) null, 
        lat REAL null, 
        lon REAL null,
        cik_number varchar(10) null,
        company_name varchar(60) null,
        mailing_formatted varchar(250) null,
        company_formatted varchar(250) null,
        distance Real null
        )
    """
    cursor.execute(sql)
    sql = """
        INSERT INTO CompanyMailingAddress
        Select
            en.street_address,
            en.city,
            en.state,
            en.zip_code,
            en.lat,
            en.lon,
            sec.cik_number,
            sec.company_name, 
            en.formatted_address,
            sec.formatted_address,
            (6371 * acos( cos( radians(en.lat) ) * 
            cos( radians( sec.latitude ) ) 
            * cos( radians( sec.longitude ) - radians(en.lon) ) 
            + sin( radians(en.lat) ) * sin(radians( sec.latitude)) ) ) AS distance 
        From EntityMailingAddress as en, bds_location as sec
        Where (6371 * acos( cos( radians(en.lat) ) * 
            cos( radians( sec.latitude ) ) 
            * cos( radians( sec.longitude ) - radians(en.lon) ) 
            + sin( radians(en.lat) ) * sin(radians( sec.latitude)) ) ) < 0.15
        Order by street_address, city, state, zip_code, distance
    """
    cursor.execute(sql)
    connection.commit()

# Match mailing address with company by formatted address (both formatted by google map api)
# This approach gives an accurate match
# But it only get 23 broker dealer. None of them bigname
def createCompanyMailingFormatted(connection, cursor):
    sql = """DROP TABLE IF EXISTS CompanyMailingFormatted"""
    cursor.execute(sql)
    sql = """
       CREATE TABLE CompanyMailingFormatted (
        street_address varchar(60) null, 
        city varchar(40) null, 
        state varchar(20) null, 
        zip_code varchar(9) null, 
        cik_number varchar(10) null,
        company_name varchar(60) null,
        mailing_formatted varchar(250) null,
        company_formatted varchar(250) null
        )
    """
    cursor.execute(sql)
    sql = """
        INSERT INTO CompanyMailingFormatted
        Select 
            en.street_address,
            en.city,
            en.state,
            en.zip_code,
            sec.cik_number,
            sec.company_name, 
            en.formatted_address,
            sec.formatted_address
        From EntityMailingAddress as en, bds_location as sec
        Where en.formatted_address Like sec.formatted_address
    """
    cursor.execute(sql)
    connection.commit()

# Broker dealers can be in the same building and this cause false positive
# But it is also possible that they have several rooms and register different rooms in different table
# The field of mailing_formatted and company_formatted and emails can be used for sanity check.
# For example
# Jump trading in SEC:          '600 W Chicago Ave #600, Chicago, IL 60654, USA'
#              in Google Map:   '600 W Chicago Ave #825, Chicago, IL 60654'
# So in FCC                     '600 W Chicago Ave #610, Chicago, IL 60654, USA'   
#                               '600 W Chicago Ave #840, Chicago, IL 60654, USA'   
#              probably belong to Jump trading
# Meanwhile
#                               '600 W Chicago Ave, Suite RW4, Chicago, IL 60654, USA'
#              shoudl also belong to Jump Trading as well
#              since it use same email as the above two addresses                                    

def createEntityCompanyTable(connection, cursor):
    sql = """DROP TABLE IF EXISTS EntityCompany"""
    cursor.execute(sql)
    sql =  """
        CREATE TABLE EntityCompany (
            call_sign char(10),
            street_address varchar(60) null, 
            city varchar(40) null, 
            state varchar(20) null, 
            zip_code varchar(9) null, 
            email varchar(50) null,
            cik_number varchar(10) null,
            company_name varchar(60) null,
            mailing_formatted varchar(250) null,
            company_formatted varchar(250) null,
            distance Real null)"""
    cursor.execute(sql)
    sql =  """
        INSERT INTO EntityCompany
        Select en.call_sign, en.street_address, en.city, en.state, en.zip_code, en.email, con.cik_number, 
        con.company_name, mailing_formatted, company_formatted, con.distance
        From licensee_entities as en, CompanyMailingAddress as con
        Where en.street_address = con.street_address and en.city = con.city and en.state = con.state and en.zip_code = con.zip_code;
        """
    cursor.execute(sql)
    connection.commit()




def main():
    connection = connect()
    cursor = connection.cursor()
    query_filer_entity = """
        SELECT Count(*) as count, street_address, city, state, zip_code
        from licensee_entities 
        where street_address is not null
        Group by street_address, city, state, zip_code Having count > 0
    """
    cursor.execute(query_filer_entity)
    mailingAddress = cursor.fetchall()
    loadEntityTable(connection, cursor, mailingAddress)

    createCompanyMailingTable(connection, cursor)
    createEntityCompanyTable(connection, cursor)
    connection.close()
    
main()
    