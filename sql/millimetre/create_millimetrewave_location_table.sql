use python_fcc_db_test;

select 'Dropping and recreating millimetrewave_location table' as '';
drop table if exists millimetrewave_location;
create table millimetrewave_location
(
      record_type               char(2)              not null,
      unique_system_identifier  numeric(9,0)         not null,
      uls_file_number           char(14)             null,
      ebf_number                varchar(30)          null,
      call_sign                 char(10)             null,
      location_action_performed char(1)              null,
      location_type_code        char(1)              null,
      location_class_code       char(1)              null,
      location_number           int                  null,
      site_status               char(1)              null,
      corresponding_fixed_location int               null,
      location_address          varchar(80)          null,
      location_city             char(20)             null,
      location_county           varchar(60)          null,
      location_state            char(2)              null,
      radius_of_operation       numeric(5,1)         null,
      area_of_operation_code    char(1)              null,
      clearance_indicator       char(1)              null,
      ground_elevation          numeric(7,1)         null,
      lat_degrees               int                  null,
      lat_minutes               int                  null,
      lat_seconds               numeric(3,1)         null,
      lat_direction             char(1)              null,
      long_degrees              int                  null,
      long_minutes              int                  null,
      long_seconds              numeric(3,1)         null,
      long_direction            char(1)              null,
      max_lat_degrees           int                  null,
      max_lat_minutes           int                  null,
      max_lat_seconds           numeric(3,1)         null,
      max_lat_direction         char(1)              null,
      max_long_degrees          int                  null,
      max_long_minutes          int                  null,
      max_long_seconds          numeric(3,1)         null,
      max_long_direction        char(1)              null,
      nepa                      char(1)              null,
      quiet_zone_notification_date char(10)          null,
      tower_registration_number char(10)             null,
      height_of_support_structure numeric(7,1)       null,
      overall_height_of_structure numeric(7,1)       null,
      structure_type            char(7)              null,
      airport_id                char(4)              null,
      location_name             char(20)             null,
      units_hand_held           int                  null,
      units_mobile              int                  null,
      units_temp_fixed          int                  null,
      units_aircraft            int                  null,
      units_itinerant           int                  null,
      status_code		char(1)		     null,
      status_date		datetime	     null,
      earth_agree               char(1)              null

);



select 'Loading millimetrewave_location table from /vagrant/data/LO.dat' as '';
load data infile '/vagrant/data/l_7090/LO.DAT'
ignore
into table millimetrewave_location fields terminated by '|'  ENCLOSED BY '' ESCAPED BY '' lines terminated by '\n'
(@record_type,@unique_system_identifier,@uls_file_number,@ebf_number,@call_sign,@location_action_performed,@location_type_code,@location_class_code,@location_number,@site_status,@corresponding_fixed_location,@location_address,@location_city,@location_county,@location_state,@radius_of_operation,@area_of_operation_code,@clearance_indicator,@ground_elevation,@lat_degrees,@lat_minutes,@lat_seconds,@lat_direction,@long_degrees,@long_minutes,@long_seconds,@long_direction,@max_lat_degrees,@max_lat_minutes,@max_lat_seconds,@max_lat_direction,@max_long_degrees,@max_long_minutes,@max_long_seconds,@max_long_direction,@nepa,@quiet_zone_notification_date,@tower_registration_number,@height_of_support_structure,@overall_height_of_structure,@structure_type,@airport_id,@location_name,@units_hand_held,@units_mobile,@units_temp_fixed,@units_aircraft,@units_itinerant,@status_code,@status_date,@earth_agree) set
record_type = if(@record_type='', null, @record_type),
unique_system_identifier = if(@unique_system_identifier='', null, @unique_system_identifier),
uls_file_number = if(@uls_file_number='', null, @uls_file_number),
ebf_number = if(@ebf_number='', null, @ebf_number),
call_sign = if(@call_sign='', null, @call_sign),
location_action_performed = if(@location_action_performed='', null, @location_action_performed),
location_type_code = if(@location_type_code='', null, @location_type_code),
location_class_code = if(@location_class_code='', null, @location_class_code),
location_number = if(@location_number='', null, @location_number),
site_status = if(@site_status='', null, @site_status),
corresponding_fixed_location = if(@corresponding_fixed_location='', null, @corresponding_fixed_location),
location_address = if(@location_address='', null, @location_address),
location_city = if(@location_city='', null, @location_city),
location_county = if(@location_county='', null, @location_county),
location_state = if(@location_state='', null, @location_state),
radius_of_operation = if(@radius_of_operation='', null, @radius_of_operation),
area_of_operation_code = if(@area_of_operation_code='', null, @area_of_operation_code),
clearance_indicator = if(@clearance_indicator='', null, @clearance_indicator),
ground_elevation = if(@ground_elevation='', 0.0, @ground_elevation),
lat_degrees = if(@lat_degrees='', null, @lat_degrees),
lat_minutes = if(@lat_minutes='', null, @lat_minutes),
lat_seconds = if(@lat_seconds='', null, @lat_seconds),
lat_direction = if(@lat_direction='', null, @lat_direction),
long_degrees = if(@long_degrees='', null, @long_degrees),
long_minutes = if(@long_minutes='', null, @long_minutes),
long_seconds = if(@long_seconds='', null, @long_seconds),
long_direction = if(@long_direction='', null, @long_direction),
max_lat_degrees = if(@max_lat_degrees='', null, @max_lat_degrees),
max_lat_minutes = if(@max_lat_minutes='', null, @max_lat_minutes),
max_lat_seconds = if(@max_lat_seconds='', null, @max_lat_seconds),
max_lat_direction = if(@max_lat_direction='', null, @max_lat_direction),
max_long_degrees = if(@max_long_degrees='', null, @max_long_degrees),
max_long_minutes = if(@max_long_minutes='', null, @max_long_minutes),
max_long_seconds = if(@max_long_seconds='', null, @max_long_seconds),
max_long_direction = if(@max_long_direction='', null, @max_long_direction),
nepa = if(@nepa='', null, @nepa),
quiet_zone_notification_date = if(@quiet_zone_notification_date='', null, @quiet_zone_notification_date),
tower_registration_number = if(@tower_registration_number='', null, @tower_registration_number),
height_of_support_structure = if(@height_of_support_structure='', null, @height_of_support_structure),
overall_height_of_structure = if(@overall_height_of_structure='', null, @overall_height_of_structure),
structure_type = if(@structure_type='', null, @structure_type),
airport_id = if(@airport_id='', null, @airport_id),
location_name = if(@location_name='', null, @location_name),
units_hand_held = if(@units_hand_held='', null, @units_hand_held),
units_mobile = if(@units_mobile='', null, @units_mobile),
units_temp_fixed = if(@units_temp_fixed='', null, @units_temp_fixed),
units_aircraft = if(@units_aircraft='', null, @units_aircraft),
units_itinerant = if(@units_itinerant='', null, @units_itinerant),
status_code = if(@status_code='', null, @status_code),
status_date = if(@status_date='', null, @status_date),
earth_agree = if(@earth_agree='', null, @earth_agree)
;

select 'Adding index to (call_sign, location_number) on millimetrewave_location table' as '';
alter table millimetrewave_location add index callsign_location_num_index(call_sign, location_number) USING BTREE ; /* use BTREE instead of default HASH, else queries only requiring just callsign wont be able to use index */

select 'Adding column lat_long_point and setting its value on millimetrewave_location table' as '';
alter table millimetrewave_location add column lat_long_point POINT;
update millimetrewave_location 
	set 
		lat_long_point = POINT(
			IF(long_direction='W',-1.0,1.0) * (long_degrees + (long_minutes/60.0) + (long_seconds/3600.0)),
			IF(lat_direction ='S',-1.0,1.0) * (lat_degrees + (lat_minutes/60.0) + (lat_seconds/3600.0))
            );

ALTER TABLE millimetrewave_location
DROP COLUMN uls_file_number,
DROP COLUMN ebf_number,
DROP COLUMN location_action_performed,
DROP COLUMN corresponding_fixed_location,
DROP COLUMN units_hand_held,
DROP COLUMN units_mobile,
DROP COLUMN units_temp_fixed,
DROP COLUMN units_aircraft,
DROP COLUMN units_itinerant,
DROP COLUMN status_code,
DROP COLUMN status_date,
DROP COLUMN earth_agree
