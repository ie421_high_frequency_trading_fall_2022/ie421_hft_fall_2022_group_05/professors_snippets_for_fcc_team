select 'Dropping python_fcc_db_test if it already exists and then creating' as '';

drop database if exists python_fcc_db_test;
create database python_fcc_db_test;
use python_fcc_db_test;


select 'Dropping and then recreating python_fcc_db user' as '';
drop user if exists python_fcc_db;
SET GLOBAL validate_password.policy=LOW;
FLUSH PRIVILEGES;
drop user if exists 'python_fcc_db'@'localhost';
create user 'python_fcc_db'@'localhost' identified by 'python_fcc_db';


select 'Enabling global_local_infile to ON and disabling SQL_SAFE_UPDATES' as '';
/*show variables like 'local_infile';*/
set global local_infile = 'ON';
SET SQL_SAFE_UPDATES=0; /*Allows updates without the where clause for example*/
