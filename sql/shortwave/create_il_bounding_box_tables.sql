USE python_fcc_db_test;

DROP TABLE IF EXISTS il_bounding_box_filtered_fcc_antennas;
CREATE TABLE il_bounding_box_filtered_fcc_antennas
select * 
from fcc_location as tlo
where 
st_y(tlo.lat_long_point) is not null
and st_x(tlo.lat_long_point) is not null
and st_y(tlo.lat_long_point) < 43.7844
and st_y(tlo.lat_long_point) > 37.5934
and st_x(tlo.lat_long_point) > -94.5786
and st_x(tlo.lat_long_point) < -86.15181;

DROP TABLE IF EXISTS il_bounding_box_filtered_oet_antennas;
CREATE TABLE il_bounding_box_filtered_oet_antennas
select * 
from oet_shortwave as oet
where 
st_y(oet.lat_long_point) is not null
and st_x(oet.lat_long_point) is not null
and st_y(oet.lat_long_point) < 43.7844
and st_y(oet.lat_long_point) > 37.5934
and st_x(oet.lat_long_point) > -94.5786
and st_x(oet.lat_long_point) < -86.15181;