USE python_fcc_db_test;

DROP TABLE IF EXISTS shortwave_antenna_pairs;
CREATE TABLE shortwave_antenna_pairs
SELECT distinct
    en.entity_name as fcc_entity_name,
	ant.call_sign as fcc_callsign,
	st_x(tlo.lat_long_point) as fcc_lon_decimal,
	st_y(tlo.lat_long_point) as fcc_lat_decimal,
    oet.licensee as oet_entity_name,
	oet.callsign as oet_callsign,
	st_x(oet.lat_long_point) as oet_lon_decimal,
	st_y(oet.lat_long_point) as oet_lat_decimal
FROM 
    licensee_entities as en,
	fcc_antennas as ant, 
	il_bounding_box_filtered_fcc_antennas as tlo, 
	il_bounding_box_filtered_oet_antennas as oet
WHERE 
	tlo.lat_long_point IS NOT NULL
	AND ant.call_sign = tlo.call_sign
	AND ant.location_number = tlo.location_number
    and en.call_sign = ant.call_sign
	AND ABS(st_x(tlo.lat_long_point) - st_x(oet.lat_long_point)) <= 0.00504505
	AND ABS(st_y(tlo.lat_long_point) - st_y(oet.lat_long_point)) <= 0.00504505
;