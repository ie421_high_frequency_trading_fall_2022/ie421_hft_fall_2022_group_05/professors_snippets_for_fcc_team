USE python_fcc_db_test;

DROP TABLE IF EXISTS nj_bounding_box_filtered_fcc_antennas;
CREATE TABLE nj_bounding_box_filtered_fcc_antennas
select * 
from fcc_location as tlo
where 
st_y(tlo.lat_long_point) is not null
and st_x(tlo.lat_long_point) is not null
and st_y(tlo.lat_long_point) < 41.387666
and st_y(tlo.lat_long_point) > 40.352178
and st_x(tlo.lat_long_point) > -75.120986
and st_x(tlo.lat_long_point) < -74.076003;

DROP TABLE IF EXISTS nj_bounding_box_filtered_oet_antennas;
CREATE TABLE nj_bounding_box_filtered_oet_antennas
select * 
from oet_shortwave as oet
where 
st_y(oet.lat_long_point) is not null
and st_x(oet.lat_long_point) is not null
and st_y(oet.lat_long_point) < 41.387666
and st_y(oet.lat_long_point) > 40.352178
and st_x(oet.lat_long_point) > -75.120986
and st_x(oet.lat_long_point) < -74.076003;
