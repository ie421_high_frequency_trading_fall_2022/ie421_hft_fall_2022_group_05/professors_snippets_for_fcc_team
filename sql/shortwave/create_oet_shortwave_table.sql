use python_fcc_db_test;

select 'Dropping and recreating oet_shortwave table' as '';
drop table if exists oet_shortwave;
create table oet_shortwave
(
    callsign                    varchar(10)             null,
    file_number                 varchar(20)             null,
    shortwave_description       varchar(20)             null,
    radio_service_code          varchar(2)              null,
    licensee                    varchar(50)             null,
    frn                         int                     null,
    transmitter_city            varchar(50)             null,
    transmitter_state           char(2)                 null,
    county                      varchar(50)             null,
    street_address              varchar(60)             null,
    mobile_latitude_degrees     int                     null,
    mobile_latitude_minutes     int                     null,
    mobile_latitude_seconds     int                     null,
    mobile_latitude_direction   char(1)                 null,
    mobile_longitude_degrees    int                     null,
    mobile_longitude_minutes    int                     null,
    mobile_longitude_seconds    int                     null,
    mobile_longitude_direction  char(1)                null,
    fixed_latitude_degrees      int                     null,
    fixed_latitude_minutes      int                     null,
    fixed_latitude_seconds      int                     null,
    fixed_latitude_direction    char(1)                 null,
    fixed_longitude_degrees     int                     null,
    fixed_longitude_minutes     int                     null,
    fixed_longitude_seconds     int                     null,
    fixed_longitude_direction   char(1)                 null,
    lower_frequency             float                   null,
    upper_frequency             float                   null,
    frequency_type              char(1)                 null
);

select 'Loading oet_shortwave table from /vagrant/shortwave_data/OET_shortwave.dat' as '';
load data infile '/vagrant/shortwave_data/OET_shortwave.dat'
ignore
into table oet_shortwave fields terminated by '|' ENCLOSED BY '' ESCAPED BY '' lines terminated by '\n'
(@callsign, @file_number, @shortwave_description, @radio_service_code, @licensee, @frn, @issue_date, @expiration_date, @transmitter_city, @transmitter_state, @county, @street_address, @mobile_latitude_degrees, @mobile_latitude_minutes, @mobile_latitude_seconds, @mobile_latitude_direction, @mobile_longitude_degrees, @mobile_longitude_minutes, @mobile_longitude_seconds, @mobile_longitude_direction, @mobile_distance_center, @mobile_azimuth_center, @fixed_latitude_degrees, @fixed_latitude_minutes, @fixed_latitude_seconds, @fixed_latitude_direction, @fixed_longitude_degrees, @fixed_longitude_minutes, @fixed_longitude_seconds, @fixed_longitude_direction, @fixed_distance_center, @fixed_azimuth_center, @lower_frequency, @upper_frequency, @frequency_type) set
callsign = if(@callsign='', null, @callsign),
file_number = if(@file_number='', null, @file_number),
shortwave_description = if(@shortwave_description='', null, @shortwave_description),
radio_service_code = if(@radio_service_code='', null, @radio_service_code),
licensee = if(@licensee='', null, @licensee),
frn = if(@frn='', null, @frn),
transmitter_city = if(@transmitter_city='', null, @transmitter_city),
transmitter_state = if(@transmitter_state='', null, @transmitter_state),
county = if(@county='', null, @county),
street_address = if(@street_address='', null, @street_address),
mobile_latitude_degrees = if(@mobile_latitude_degrees='', null, @mobile_latitude_degrees),
mobile_latitude_minutes = if(@mobile_latitude_minutes='', null, @mobile_latitude_minutes),
mobile_latitude_seconds = if(@mobile_latitude_seconds='', null, @mobile_latitude_seconds),
mobile_latitude_direction = if(@mobile_latitude_direction='', null, @mobile_latitude_direction),
mobile_longitude_degrees = if(@mobile_longitude_degrees='', null, @mobile_longitude_degrees),
mobile_longitude_minutes = if(@mobile_longitude_minutes='', null, @mobile_longitude_minutes),
mobile_longitude_seconds = if(@mobile_longitude_seconds='', null, @mobile_longitude_seconds),
mobile_longitude_direction = if(@mobile_longitude_direction='', null, @mobile_longitude_direction),
fixed_latitude_degrees = if(@fixed_latitude_degrees='', null, @fixed_latitude_degrees),
fixed_latitude_minutes = if(@fixed_latitude_minutes='', null, @fixed_latitude_minutes),
fixed_latitude_seconds = if(@fixed_latitude_seconds='', null, @fixed_latitude_seconds),
fixed_latitude_direction = if(@fixed_latitude_direction='', null, @fixed_latitude_direction),
fixed_longitude_degrees = if(@fixed_longitude_degrees='', null, @fixed_longitude_degrees),
fixed_longitude_minutes = if(@fixed_longitude_minutes='', null, @fixed_longitude_minutes),
fixed_longitude_seconds = if(@fixed_longitude_seconds='', null, @fixed_longitude_seconds),
fixed_longitude_direction = if(@fixed_longitude_direction='', null, @fixed_longitude_direction),
lower_frequency = if(@lower_frequency='', null, @lower_frequency),
upper_frequency = if(@upper_frequency='', null, @upper_frequency),
frequency_type = if(@frequency_type='', null, @frequency_type)
;

select 'Adding column lat_long_point and setting its value on shortwave table' as '';
alter table oet_shortwave add column lat_long_point POINT;
update oet_shortwave 
	set 
		lat_long_point = POINT(
			IF(fixed_longitude_direction='W',-1.0,1.0) * (fixed_longitude_degrees + (fixed_longitude_minutes/60.0) + (fixed_longitude_seconds/3600.0)),
			IF(fixed_latitude_direction ='S',-1.0,1.0) * (fixed_latitude_degrees + (fixed_latitude_minutes/60.0) + (fixed_latitude_seconds/3600.0))
            );