use python_fcc_db_test;

SELECT 'Creating fcc_antennas table' as '';
DROP TABLE IF EXISTS fcc_antennas;
CREATE TABLE fcc_antennas
(
      record_type              	char(2)              null,
      unique_system_identifier  numeric(9,0)         not null,
      uls_file_number           char(14)             null,
      ebf_number                varchar(30)          null,
      call_sign                	char(10)             null,
      antenna_action_performed  char(1)              null,
      antenna_number            int                  null,
      location_number           int                  null,
      receive_zone_code         char(6)              null,
      antenna_type_code         char(1)              null,
      height_to_tip            	numeric(5,1)         null,
      height_to_center_raat     numeric(5,1)         null,
      antenna_make              varchar(25)          null,
      antenna_model             varchar(25)          null,
      tilt                      numeric(3,1)         null,
      polarization_code         char(5)              null,
      beamwidth                 numeric(4,1)         null,
      gain                      numeric(4,1)         null,
      azimuth                   numeric(4,1)         null,
      height_above_avg_terrain  numeric(5,1)         null,
      diversity_height          numeric(5,1)         null,
      diversity_gain            numeric(4,1)         null,
      diversity_beam            numeric(4,1)         null,
      reflector_height          numeric(5,1)         null,
      reflector_width           numeric(4,1)         null,
      reflector_separation      numeric(5,1)         null,
      repeater_seq_num          int                  null,
      back_to_back_tx_dish_gain numeric(4,1)         null,
      back_to_back_rx_dish_gain numeric(4,1)         null,
      location_name             varchar(20)          null,
      passive_repeater_id       int                  null,
      alternative_cgsa_method   char(1)              null,
      path_number               int                  null,
      line_loss                 numeric(3,1)         null,
      status_code		char(1)		     null,
      status_date		datetime	     null,
      psd_nonpsd_methodology    varchar(10)          null,
      maximum_erp               numeric(15,3)        null
);

SELECT 'Loading fcc_antennas table from /vagrant/data/AN.dat' as '';
LOAD DATA infile '/vagrant/data/fcc_l_micro/AN.dat'
IGNORE
INTO TABLE fcc_antennas fields terminated by '|'  ENCLOSED BY '' ESCAPED BY '' lines terminated by '\n'
(@record_type,@unique_system_identifier,@uls_file_number,@ebf_number,@call_sign,@antenna_action_performed,@location_number,@receive_zone_code,@antenna_type_code,@height_to_tip,@height_to_center_raat,@antenna_make,@antenna_model,@tilt,@polarization_code,@beamwidth,@gain,@azimuth,@height_above_avg_,@diversity_height,@diversity_gain,@diversity_beam,@reflector_height,@reflector_width,@reflector_separat,@repeater_seq_num ,@back_to_back_tx_d,@back_to_back_rx_d,@location_name,@passive_repeater_,@alternative_cgsa_,@path_number,@line_loss,@status_code,@status_date,@psd_nonpsd_method,@maximum_erp)
set 
record_type = if(@record_type='', null, @record_type),
unique_system_identifier = if(@unique_system_identifier='', null, @unique_system_identifier),
uls_file_number = if(@uls_file_number='', null, @uls_file_number),
ebf_number = if(@ebf_number='', null, @ebf_number),
call_sign = if(@call_sign='', null, @call_sign),
antenna_action_performed = if(@antenna_action_performed='', null, @antenna_action_performed),
antenna_number = if(@antenna_number='', null, @antenna_number),
location_number = if(@location_number='', null, @location_number),
receive_zone_code = if(@receive_zone_code='', null, @receive_zone_code),
antenna_type_code = if(@antenna_type_code='', null, @antenna_type_code),
height_to_tip = if(@height_to_tip='', null, @height_to_tip),
height_to_center_raat = if(@height_to_center_raat='', null, @height_to_center_raat),
antenna_make = if(@antenna_make='', null, @antenna_make),
antenna_model = if(@antenna_model='', null, @antenna_model),
tilt = if(@tilt='', null, @tilt),
polarization_code = if(@polarization_code='', null, @polarization_code),
beamwidth = if(@beamwidth='', null, @beamwidth),
gain = if(@gain='', null, @gain),
azimuth = if(@azimuth='', null, @azimuth),
height_above_avg_terrain = if(@height_above_avg_terrain='', null, @height_above_avg_terrain),
diversity_height = if(@diversity_height='', null, @diversity_height),
diversity_gain = if(@diversity_gain='', null, @diversity_gain),
diversity_beam = if(@diversity_beam='', null, @diversity_beam),
reflector_height = if(@reflector_height='', null, @reflector_height),
reflector_width = if(@reflector_width='', null, @reflector_width),
reflector_separation = if(@reflector_separation='', null, @reflector_separation),
repeater_seq_num = if(@repeater_seq_num='', null, @repeater_seq_num),
back_to_back_tx_dish_gain = if(@back_to_back_tx_dish_gain='', null, @back_to_back_tx_dish_gain),
back_to_back_rx_dish_gain = if(@back_to_back_rx_dish_gain='', null, @back_to_back_rx_dish_gain),
location_name = if(@location_name='', null, @location_name),
passive_repeater_id = if(@passive_repeater_id='', null, @passive_repeater_id),
alternative_cgsa_method = if(@alternative_cgsa_method='', null, @alternative_cgsa_method),
path_number = if(@path_number='', null, @path_number),
line_loss = if(@line_loss='', null, @line_loss),
status_code = if(@status_code='', null, @status_code),
status_date = if(@status_date='', null, @status_date),
psd_nonpsd_methodology = if(@psd_nonpsd_methodology='', null, @psd_nonpsd_methodology),
maximum_erp = if(@maximum_erp='', null, @maximum_erp)
;


SELECT 'Creating antennas table' as '';
DROP TABLE IF EXISTS millimetre_antennas;
CREATE TABLE millimetre_antennas
(
      record_type              	char(2)              null,
      unique_system_identifier  numeric(9,0)         not null,
      uls_file_number           char(14)             null,
      ebf_number                varchar(30)          null,
      call_sign                	char(10)             null,
      antenna_action_performed  char(1)              null,
      antenna_number            int                  null,
      location_number           int                  null,
      receive_zone_code         char(6)              null,
      antenna_type_code         char(1)              null,
      height_to_tip            	numeric(5,1)         null,
      height_to_center_raat     numeric(5,1)         null,
      antenna_make              varchar(25)          null,
      antenna_model             varchar(25)          null,
      tilt                      numeric(3,1)         null,
      polarization_code         char(5)              null,
      beamwidth                 numeric(4,1)         null,
      gain                      numeric(4,1)         null,
      azimuth                   numeric(4,1)         null,
      height_above_avg_terrain  numeric(5,1)         null,
      diversity_height          numeric(5,1)         null,
      diversity_gain            numeric(4,1)         null,
      diversity_beam            numeric(4,1)         null,
      reflector_height          numeric(5,1)         null,
      reflector_width           numeric(4,1)         null,
      reflector_separation      numeric(5,1)         null,
      repeater_seq_num          int                  null,
      back_to_back_tx_dish_gain numeric(4,1)         null,
      back_to_back_rx_dish_gain numeric(4,1)         null,
      location_name             varchar(20)          null,
      passive_repeater_id       int                  null,
      alternative_cgsa_method   char(1)              null,
      path_number               int                  null,
      line_loss                 numeric(3,1)         null,
      status_code		char(1)		     null,
      status_date		datetime	     null,
      psd_nonpsd_methodology    varchar(10)          null,
      maximum_erp               numeric(15,3)        null
);

SELECT 'Loading millimetre_antennas table from /vagrant/data/AN.dat' as '';
LOAD DATA infile '/vagrant/data/l_7090/AN.DAT'
IGNORE
INTO TABLE millimetre_antennas fields terminated by '|'  ENCLOSED BY '' ESCAPED BY '' lines terminated by '\n'
(@record_type,@unique_system_identifier,@uls_file_number,@ebf_number,@call_sign,@antenna_action_performed,@location_number,@receive_zone_code,@antenna_type_code,@height_to_tip,@height_to_center_raat,@antenna_make,@antenna_model,@tilt,@polarization_code,@beamwidth,@gain,@azimuth,@height_above_avg_,@diversity_height,@diversity_gain,@diversity_beam,@reflector_height,@reflector_width,@reflector_separat,@repeater_seq_num ,@back_to_back_tx_d,@back_to_back_rx_d,@location_name,@passive_repeater_,@alternative_cgsa_,@path_number,@line_loss,@status_code,@status_date,@psd_nonpsd_method,@maximum_erp)
set 
record_type = if(@record_type='', null, @record_type),
unique_system_identifier = if(@unique_system_identifier='', null, @unique_system_identifier),
uls_file_number = if(@uls_file_number='', null, @uls_file_number),
ebf_number = if(@ebf_number='', null, @ebf_number),
call_sign = if(@call_sign='', null, @call_sign),
antenna_action_performed = if(@antenna_action_performed='', null, @antenna_action_performed),
antenna_number = if(@antenna_number='', null, @antenna_number),
location_number = if(@location_number='', null, @location_number),
receive_zone_code = if(@receive_zone_code='', null, @receive_zone_code),
antenna_type_code = if(@antenna_type_code='', null, @antenna_type_code),
height_to_tip = if(@height_to_tip='', null, @height_to_tip),
height_to_center_raat = if(@height_to_center_raat='', null, @height_to_center_raat),
antenna_make = if(@antenna_make='', null, @antenna_make),
antenna_model = if(@antenna_model='', null, @antenna_model),
tilt = if(@tilt='', null, @tilt),
polarization_code = if(@polarization_code='', null, @polarization_code),
beamwidth = if(@beamwidth='', null, @beamwidth),
gain = if(@gain='', null, @gain),
azimuth = if(@azimuth='', null, @azimuth),
height_above_avg_terrain = if(@height_above_avg_terrain='', null, @height_above_avg_terrain),
diversity_height = if(@diversity_height='', null, @diversity_height),
diversity_gain = if(@diversity_gain='', null, @diversity_gain),
diversity_beam = if(@diversity_beam='', null, @diversity_beam),
reflector_height = if(@reflector_height='', null, @reflector_height),
reflector_width = if(@reflector_width='', null, @reflector_width),
reflector_separation = if(@reflector_separation='', null, @reflector_separation),
repeater_seq_num = if(@repeater_seq_num='', null, @repeater_seq_num),
back_to_back_tx_dish_gain = if(@back_to_back_tx_dish_gain='', null, @back_to_back_tx_dish_gain),
back_to_back_rx_dish_gain = if(@back_to_back_rx_dish_gain='', null, @back_to_back_rx_dish_gain),
location_name = if(@location_name='', null, @location_name),
passive_repeater_id = if(@passive_repeater_id='', null, @passive_repeater_id),
alternative_cgsa_method = if(@alternative_cgsa_method='', null, @alternative_cgsa_method),
path_number = if(@path_number='', null, @path_number),
line_loss = if(@line_loss='', null, @line_loss),
status_code = if(@status_code='', null, @status_code),
status_date = if(@status_date='', null, @status_date),
psd_nonpsd_methodology = if(@psd_nonpsd_methodology='', null, @psd_nonpsd_methodology),
maximum_erp = if(@maximum_erp='', null, @maximum_erp)
;

select 'Adding index to (call_sign, location_number) on millimetre_antenna table' as '';
alter table millimetre_antennas add index callsign_location_num_index(call_sign, location_number) USING BTREE ; /* use BTREE instead of default HASH, else queries only requiring just callsign wont be able to use index */




