use python_fcc_db_test;

select 'Finished loading the data, now putting it into SQL table' as '';
DROP TABLE IF EXISTS part1table;
CREATE TABLE part1table (
  call_sign VARCHAR(32) NOT NULL,
  email VARCHAR(100) NOT NULL,
  lat REAL NOT NULL,
  lon REAL NOT NULL
);

LOAD DATA INFILE '/vagrant/google_maps_SEC/src/part1Results/final.csv'
INTO TABLE part1table
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' lines terminated by '\n'
IGNORE 1 LINES
(@ids, @call_sign, @email, @lat, @lon, @company_name)
set 
call_sign = @call_sign,
email = @email,
lat = @lat,
lon = @lon;