use python_fcc_db_test;

Drop Table if Exists part1table;
CREATE TABLE part1table(
 call_sign char(10) null,
 email varchar(50) null,
 lat REAL null,
 lon REAL null
);

LOAD DATA INFILE '/vagrant/data/part1table.csv'
INTO TABLE part1table
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' lines terminated by '\n'
IGNORE 1 LINES
(@ids, @call_sign, @email, @lat, @lon)
set 
call_sign = @call_sign,
email = @email,
lat = @lat,
lon = @lon;

Drop Table if Exists part1tabledistinct;
CREATE TABLE part1tabledistinct(
 call_sign char(10) null,
 email varchar(50) null,
 lat REAL null,
 lon REAL null
);
Insert Into part1tabledistinct
select distinct call_sign, email, lat, lon from part1table

Drop Table if Exists path_company;
CREATE TABLE path_company(
 call_sign char(10) null,
 email varchar(50) null,
 lat REAL null,
 lon REAL null,
 company_name varchar(60) null,
 cik_number varchar(10) null,
 mailing_formatted varchar(250),
 company_formatted varchar(250),
 distance Real
);

Insert into path_company
select part1tabledistinct.call_sign as call_sign,   
part1tabledistinct.email as email,
part1tabledistinct.lat as lat,
part1tabledistinct.lon as lon,
entity_company.company_name as company_name,
entity_company.cik_number as cik_number,
entity_company.mailing_formatted as mailing_formatted,
entity_company.company_formatted as company_formatted,
entity_company.distance as distance
from part1tabledistinct left join entity_company on part1tabledistinct.call_sign = entity_company.call_sign
Order by call_sign, company_name, distance

Select * from path_company
INTO OUTFILE '/vagrant/data/path_company.csv'
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
Lines terminated by '\r\n';

