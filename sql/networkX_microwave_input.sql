select
    distinct 
    en.call_sign as call_sign,
    en.email as email,
    en.company_name as company_name,
    en.mailing_formatted as mailing_formatted,
    en.company_formatted as company_formatted,
    en.distance as distance,
    st_x(tlo.lat_long_point) as lon_decimal,
    st_y(tlo.lat_long_point) as lat_decimal,
    tlo.ground_elevation as t_ground_elevation,
    st_x(rlo.lat_long_point) as r_lon_decimal,
    st_y(rlo.lat_long_point) as r_lat_decimal,
    rlo.ground_elevation as r_ground_elevation,
    CONCAT(mp.unique_system_identifier, '', mp.callsign) as uis
from 
    filtered_path_company as en,
    microwave_paths as mp,
    fcc_location as tlo,
    fcc_location as rlo
where 
    en.call_sign = mp.callsign
    and tlo.call_sign = mp.callsign
    and tlo.location_number = mp.transmit_location_number
    and rlo.call_sign = mp.callsign
    and rlo.location_number = mp.receiver_location_number
    and tlo.lat_long_point IS NOT NULL
    and rlo.lat_long_point IS NOT NULL
limit 10000000;