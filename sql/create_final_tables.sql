use python_fcc_db_test;

DROP TABLE IF EXISTS microwave_finalTable1;
CREATE TABLE microwave_finalTable1 (
  path_number int NOT NULL, 
  call_sign VARCHAR(30) NOT NULL,
  t_lat REAL NOT NULL,
  t_lon REAL NOT NULL,
  r_lat REAL NOT NULL,
  r_lon REAL NOT NULL,
  t_height REAL NOT NULL,
  r_height REAL NOT NULL,
  email VARCHAR(100) NOT NULL
);

LOAD DATA 
INFILE "/vagrant/google_maps_SEC/data/networkX_results/microwave/table1.txt"
REPLACE
INTO TABLE microwave_finalTable1
FIELDS TERMINATED BY " " 
LINES terminated by "\n"
IGNORE 1 LINES;

DROP TABLE IF EXISTS microwave_finalTable2;
CREATE TABLE microwave_finalTable2 (
  path_number REAL NOT NULL, 
  email VARCHAR(100) NOT NULL,
  company VARCHAR(100) NOT NULL,
  length REAL NOT NULL,
  latency REAL NOT NULL
);

LOAD DATA 
INFILE "/vagrant/google_maps_SEC/data/networkX_results/microwave/table2.txt"
IGNORE
INTO TABLE microwave_finalTable2
FIELDS terminated by " " OPTIONALLY enclosed by '"'
LINES terminated by "\n";


DROP TABLE IF EXISTS millimetrewave_finalTable1;
CREATE TABLE millimetrewave_finalTable1 (
  path_number int NOT NULL, 
  call_sign VARCHAR(30) NOT NULL,
  t_lat REAL NOT NULL,
  t_lon REAL NOT NULL,
  r_lat REAL NOT NULL,
  r_lon REAL NOT NULL,
  t_height REAL NOT NULL,
  r_height REAL NOT NULL,
  email VARCHAR(100) NOT NULL
);

LOAD DATA 
INFILE "/vagrant/google_maps_SEC/data/networkX_results/millimetre/table1.txt"
REPLACE
INTO TABLE millimetrewave_finalTable1
FIELDS TERMINATED BY " " 
LINES terminated by "\n"
IGNORE 1 LINES;

DROP TABLE IF EXISTS millimetrewave_finalTable2;
CREATE TABLE millimetrewave_finalTable2 (
  path_number REAL NOT NULL, 
  email VARCHAR(100) NOT NULL,
  company VARCHAR(100) NOT NULL,
  length REAL NOT NULL,
  latency REAL NOT NULL
);

LOAD DATA 
INFILE "/vagrant/google_maps_SEC/data/networkX_results/millimetre/table2.txt"
IGNORE
INTO TABLE millimetrewave_finalTable2
FIELDS terminated by " " OPTIONALLY enclosed by '"'
LINES terminated by "\n";