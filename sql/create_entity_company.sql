use python_fcc_db_test;

SELECT 'Creating bds_location table' as '';
DROP TABLE IF EXISTS bds_location;
CREATE TABLE bds_location
(
    cik_number	varchar(10) null,
    company_name varchar(60) null,
    latitude float null,
    longitude float null,
    formatted_address varchar(250) null
);

SELECT 'Loading bds_location table from /vagrant/gps-data/bds_location.csv' as '';
LOAD DATA INFILE '/vagrant/gps-data/bds_location.csv'
INTO TABLE bds_location fields terminated by '|' OPTIONALLY ENCLOSED BY '"' lines terminated by '\r\n' 
(@cik_number, @company_name, @latitude, @longitude, @formatted_address)
set 
cik_number = if(@cik_number='', null, @cik_number),
company_name = if(@company_name='', null, @company_name),
latitude = if(@latitude='', null, @latitude),
longitude = if(@longitude='', null, @longitude),
formatted_address = if(@formatted_address='', null, @formatted_address)
;

SELECT 'Creating entity_mailing_address table' as '';
DROP TABLE IF EXISTS entity_mailing_address;
CREATE TABLE entity_mailing_address
(
	entity_count int, 
    street_address varchar(60) null, 
    city varchar(40) null, 
	state varchar(20) null, 
    zip_code varchar(9) null, 
    lat REAL null, 
    lon REAL null, 
    formatted_address varchar(250) null
);

SELECT 'Loading entity_mailing_address table from /vagrant/gps-data/entity_mailing_new.csv' as '';
LOAD DATA INFILE '/vagrant/gps-data/entity_mailing_new.csv'
INTO TABLE entity_mailing_address fields terminated by '|' OPTIONALLY ENCLOSED BY '"' lines terminated by '\r\n' 
(@entity_count, @street_address, @city, @state, @zip_code, @lat, @lon, @formatted_address)
set 
entity_count = if(@entity_count='', null, @entity_count),
street_address = if(@street_address='', null, @street_address),
city = if(@city='', null, @city),
state = if(@state='', null, @state),
zip_code = if(@zip_code='', null, @zip_code),
lat = if(@lat='', null, @lat),
lon = if(@lon='', null, @lon),
formatted_address = if(@formatted_address='', null, @formatted_address)
;

SELECT 'Creating company_mailing_address table' as '';
DROP TABLE IF EXISTS company_mailing_address;
CREATE TABLE company_mailing_address (
	street_address varchar(60) null, 
	city varchar(40) null, 
	state varchar(20) null, 
	zip_code varchar(9) null, 
	lat REAL null, 
	lon REAL null,
	cik_number varchar(10) null,
	company_name varchar(60) null,
	mailing_formatted varchar(250) null,
	company_formatted varchar(250) null,
	distance Real null
);
INSERT INTO company_mailing_address
Select
	en.street_address,
	en.city,
	en.state,
	en.zip_code,
	en.lat,
	en.lon,
	sec.cik_number,
	sec.company_name, 
	en.formatted_address,
	sec.formatted_address,
	(6371 * acos( cos( radians(en.lat) ) * 
	cos( radians( sec.latitude ) ) 
	* cos( radians( sec.longitude ) - radians(en.lon) ) 
	+ sin( radians(en.lat) ) * sin(radians( sec.latitude)) ) ) AS distance 
From entity_mailing_address as en, bds_location as sec
Where (6371 * acos( cos( radians(en.lat) ) * 
	cos( radians( sec.latitude ) ) 
	* cos( radians( sec.longitude ) - radians(en.lon) ) 
	+ sin( radians(en.lat) ) * sin(radians( sec.latitude)) ) ) < 0.15
Order by street_address, city, state, zip_code, distance;

SELECT 'Creating entity_company table' as '';
DROP TABLE IF EXISTS entity_company;
CREATE TABLE entity_company (
	call_sign char(10),
	street_address varchar(60) null, 
	city varchar(40) null, 
	state varchar(20) null, 
	zip_code varchar(9) null, 
	email varchar(50) null,
	cik_number varchar(10) null,
	company_name varchar(60) null,
	mailing_formatted varchar(250) null,
	company_formatted varchar(250) null,
	distance Real null
);
INSERT INTO entity_company
Select en.call_sign, en.street_address, en.city, en.state, en.zip_code, en.email, con.cik_number, 
con.company_name, mailing_formatted, company_formatted, con.distance
From licensee_entities as en, company_mailing_address as con
Where en.street_address = con.street_address and en.city = con.city and en.state = con.state and en.zip_code = con.zip_code;
