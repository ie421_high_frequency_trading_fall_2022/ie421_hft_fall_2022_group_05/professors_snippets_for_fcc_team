use python_fcc_db_test;

SELECT 'Creating bds table' as '';
DROP TABLE IF EXISTS bds;
CREATE TABLE bds 
(
	cik_number	varchar(10) PRIMARY KEY NOT NULL,
	company_name varchar(60) null,
	reporting_file_number varchar(25) null,
	address1 varchar(40) null,
	address2 varchar(40) null,
	city varchar(30) null,
	state_code varchar(2) null,
	zip_code varchar(10)
);

SELECT 'Loading bds table from /vagrant/data/bd.txt' as '';
LOAD DATA INFILE '/vagrant/data/bd.txt'
IGNORE
INTO TABLE bds fields terminated by '\t' lines terminated by '\r\n'
IGNORE 1 LINES
(@cik_number, @company_name, @reporting_file_number, @address1, @address2, @city, @state_code, @zip_code)
set 
cik_number = if(@cik_number='', null, @cik_number),
company_name = if(@company_name='', null, @company_name),
reporting_file_number = if(@reporting_file_number='', null, @reporting_file_number),
address1 = if(@address1='', null, @address1),
address2 = if(@address2='', null, @address2),
city = if(@city='', null, @city),
state_code = if(@state_code='', null, @state_code),
zip_code = if(@zip_code='', null, @zip_code)

;

select 'Adding index to (address1, city, state_code, zip_code) on location table' as '';
alter table bds add index address_index(address1, city, state_code, zip_code) USING BTREE;
