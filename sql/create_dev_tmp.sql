SELECT 'A collection of dev code. Not in prod pipeline'

use python_fcc_db_test;

DROP TABLE IF EXISTS links;
CREATE TABLE links
(
	call_sign char(10) null

);
SELECT 'Loading bds table from /vagrant/data/callsignFile.txt' as '';
LOAD DATA INFILE '/vagrant/data/callsignFile.txt'
INTO TABLE links lines terminated by '\n'
(@call_sign)
set 
call_sign = @call_sign
;

Select * from EntityMailingAddress
INTO OUTFILE '/vagrant/data/entity_mailing.csv'
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
Lines terminated by '\r\n';


DROP TABLE IF EXISTS pathlinks;
CREATE TABLE pathlinks
(
	path_number int,
	call_sign char(10) null,
    email varchar(250) null

);
SELECT 'Loading bds table from /vagrant/data/pathCallSign.txt' as '';
LOAD DATA INFILE '/vagrant/data/pathCallSign.txt'
INTO TABLE pathlinks
FIELDS TERMINATED BY ' ' lines terminated by '\n'
(@path_number, @call_sign, @email)
set 
path_number = @path_number,
call_sign = @call_sign,
email = @email
;

Drop Table if Exists allPaths;
CREATE TABLE allPaths(
 call_sign char(10) null,
 email varchar(50) null,
 lat REAL null,
 lon REAL null
);

LOAD DATA INFILE '/vagrant/data/final.csv'
INTO TABLE allPaths
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' lines terminated by '\n'
IGNORE 1 LINES
(@ids, @call_sign, @email, @lat, @lon, @company_name)
set 
call_sign = @call_sign,
email = @email,
lat = @lat,
lon = @lon;

select allPaths.call_sign as call_sign, allPaths.email as email, allPaths.lat as lat, allPaths.lon as lon,
EntityCompany.company_name as company_name, EntityCompany.cik_number as cik, 
EntityCompany.mailing_formatted as mailing_formatted, EntityCompany.company_formatted as company_formatted, EntityCompany.distance as distance
From allPaths, EntityCompany
where allPaths.call_sign = EntityCompany.call_sign and allPaths.email = EntityCompany.email
order by call_sign, EntityCompany.distance
INTO OUTFILE '/vagrant/gps-data/path_company.csv'
FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
Lines terminated by '\r\n'