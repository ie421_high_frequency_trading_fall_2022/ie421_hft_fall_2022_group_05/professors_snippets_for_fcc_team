use python_fcc_db_test;

Drop Table if Exists part1tabledistinct;
CREATE TABLE part1tabledistinct(
 call_sign char(10) null,
 email varchar(50) null,
 lat REAL null,
 lon REAL null
);
Insert Into part1tabledistinct
select distinct call_sign, email, lat, lon from part1table;

SELECT 'Creating filtered_entity_company table' as '';
DROP TABLE IF EXISTS filtered_entity_company;
CREATE TABLE filtered_entity_company (
	call_sign char(10) null,
	email varchar(50) null,
	company_name varchar(60) null,
	mailing_formatted varchar(250) null,
	company_formatted varchar(250) null,
	distance Real null
);

INSERT INTO filtered_entity_company
select entity_company.call_sign, entity_company.email, entity_company.company_name, entity_company.mailing_formatted, entity_company.company_formatted, entity_company.distance
from entity_company,
(select call_sign, MIN(distance) as distance from entity_company group by call_sign) filterTable 
where entity_company.call_sign = filterTable.call_sign and
entity_company.distance = filterTable.distance;

Drop Table if Exists filtered_path_company;
CREATE TABLE filtered_path_company(
 call_sign char(10) null,
 email varchar(50) null,
 lat REAL null,
 lon REAL null,
 company_name varchar(60) null,
 mailing_formatted varchar(250),
 company_formatted varchar(250),
 distance Real
);

Insert into filtered_path_company
select part1tabledistinct.call_sign as call_sign,   
part1tabledistinct.email as email,
part1tabledistinct.lat as lat,
part1tabledistinct.lon as lon,
ec.company_name as company_name,
ec.mailing_formatted as mailing_formatted,
ec.company_formatted as company_formatted,
ec.distance as distance
from part1tabledistinct left join filtered_entity_company as ec on part1tabledistinct.call_sign = ec.call_sign;

-- select * from filtered_path_company
-- INTO OUTFILE '/vagrant/data/filtered_path_company.csv'
-- FIELDS TERMINATED BY '|' OPTIONALLY ENCLOSED BY '"'
-- Lines terminated by '\r\n'
