use python_fcc_db_test;

select 'Dropping and recreating entities_of_interest table' as '';
drop table if exists entities_of_interest;
create table entities_of_interest as select * from entities where 
	email like '%mckay-brothers.com'
	OR email like '%wcwtech.com'
	OR email like '%geodesicnetworks.com'
	OR email like '%auburndata.com'
	OR email like '%abservicesllc.com'
	OR email like '%NeXXComwireless.com'
	OR email like '%isignalnetworks.com'
	OR email like '%anova-tech.com'
	OR email like '%infiniumcm.com'
	OR email like '%tatora.com'
	OR email like '%midwestics.com'
	OR email like '%apsaranetworks.com'
	OR email like '%bsonetwork.com'
	OR email like '%striketechnologies.com'
	OR email like '%akingump.com'
	OR email like '%surveillancetechs.com'
	OR email like '%bobbroadband.com'
	OR email like '%gammafcc@gmail.com' 
	OR email like '%newlinenet.com'  
;

