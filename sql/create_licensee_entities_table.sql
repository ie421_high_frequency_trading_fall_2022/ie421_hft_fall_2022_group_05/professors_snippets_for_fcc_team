use python_fcc_db_test;

DROP TABLE IF EXISTS licensee_entities;
CREATE TABLE licensee_entities
SELECT *
FROM entities as en
WHERE en.entity_type = "L";