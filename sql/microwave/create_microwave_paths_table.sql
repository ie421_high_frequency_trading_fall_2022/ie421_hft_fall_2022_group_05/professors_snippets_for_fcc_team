use python_fcc_db_test;

select 'Dropping and recreating microwave_paths table' as '';
drop table if exists microwave_paths;
create table microwave_paths
(
      record_type               char(2)              null,
      unique_system_identifier  numeric(9,0)         not null,
      uls_file_number           char(14)             null,
      ebf_number                varchar(30)          null,
      callsign                  char(10)             null,
      path_action_performed     char(1)              null,
      path_number               int                  null,
      transmit_location_number  int                  null,
      transmit_antenna_number   int                  null,
      receiver_location_number  int                  null,
      receiver_antenna_number   int                  null,
      mas_dems_subtype          char(2)              null,
      path_type_desc            char(20)             null,
      passive_receiver_indicator char(1)             null,
      country_code              char(3)              null,
      interference_to_gso       char(1)              null,
      receiver_callsign         varchar(10)          null,
      angular_sep		numeric(3,2)         null,
      cert_no_alternative	char(1)		     null,
      cert_no_interference	char(1)  	     null,
	  status_code			char(1)			null,
	  status_date			datetime		null,
	  link_start_date		datetime		null,
	  link_end_date			datetime		null
);


select 'Loading microwave_paths table from /vagrant/data/PA.dat' as '';
load data infile '/vagrant/data/fcc_l_micro/PA.dat'
ignore
into table microwave_paths fields terminated by '|' ENCLOSED BY '' ESCAPED BY '' lines terminated by '\n'
(@record_type,@unique_system_identifier,@uls_file_number,@ebf_number,@callsign,@path_action_performed,@path_number,@transmit_location_number,@transmit_antenna_number,@receiver_location_number,@receiver_antenna_number,@mas_dems_subtype,@path_type_desc,@passive_receiver_indicator,@country_code,@interference_to_gso,@receiver_callsign,@angular_sep,@cert_no_alternative,@cert_no_interference,@status_code,@status_date,@link_start_date,@link_end_date) 
set
record_type = if(@record_type='', null, @record_type),
unique_system_identifier = if(@unique_system_identifier='', null, @unique_system_identifier),
uls_file_number = if(@uls_file_number='', null, @uls_file_number),
ebf_number = if(@ebf_number='', null, @ebf_number),
callsign = if(@callsign='', null, @callsign),
path_action_performed = if(@path_action_performed='', null, @path_action_performed),
path_number = if(@path_number='', null, @path_number),
transmit_location_number = if(@transmit_location_number='', null, @transmit_location_number),
transmit_antenna_number = if(@transmit_antenna_number='', null, @transmit_antenna_number),
receiver_location_number = if(@receiver_location_number='', null, @receiver_location_number),
receiver_antenna_number = if(@receiver_antenna_number='', null, @receiver_antenna_number),
mas_dems_subtype = if(@mas_dems_subtype='', null, @mas_dems_subtype),
path_type_desc = if(@path_type_desc='', null, @path_type_desc),
passive_receiver_indicator = if(@passive_receiver_indicator='', null, @passive_receiver_indicator),
country_code = if(@country_code='', null, @country_code),
interference_to_gso = if(@interference_to_gso='', null, @interference_to_gso),
receiver_callsign = if(@receiver_callsign='', null, @receiver_callsign),
angular_sep = if(@angular_sep='', null, @angular_sep),
cert_no_alternative = if(@cert_no_alternative='', null, @cert_no_alternative),
cert_no_interference = if(@cert_no_interference='', null, @cert_no_interference),
status_code = if(@status_code='', null, @status_code),
status_date = if(@status_date='', null, @status_date),
link_start_date = if(@link_start_date='', null, @link_start_date),
link_end_date = if(@link_end_date='', null, @link_end_date)
;

select 'Adding index (callsign, transmit_location_number) to microwave_paths table' as '';
alter table microwave_paths add index callsign_index(callsign, transmit_location_number) USING BTREE ; /* use BTREE instead of default HASH, else queries only requiring just callsign wont be able to use index */

