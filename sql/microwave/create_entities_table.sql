use python_fcc_db_test;

select 'Creating entities table' as '';
drop table if exists entities;
create table entities
(
      record_type               char(2)              not null,
      unique_system_identifier  numeric(9,0)         not null,
      uls_file_number           char(14)             null,
      ebf_number                varchar(30)          null,
      call_sign                 char(10)             null,
      entity_type               char(2)              null,
      licensee_id               char(9)              null,
      entity_name               varchar(200)         null,
      first_name                varchar(20)          null,
      mi                        char(1)              null,
      last_name                 varchar(20)          null,
      suffix                    char(3)              null,
      phone                     char(10)             null,
      fax                       char(10)             null,
      email                     varchar(50)          null,
      street_address            varchar(60)          null,
      city                      varchar(20)          null,
      state                     char(2)              null,
      zip_code                  char(9)              null,
      po_box                    varchar(20)          null,
      attention_line            varchar(35)          null,
      sgin                      char(3)              null,
      frn                       char(10)             null,
      applicant_type_code       char(1)              null,
      applicant_type_other      char(40)             null,
      status_code               char(1)		     null,
      status_date		datetime		null,
      lic_category_code	char(1)		null,
      linked_license_id	numeric(9,0)	null,
      linked_callsign		char(10)		null
);



select 'Loading entities table from /vagrant/data/EN.dat' as '';
load data infile '/vagrant/data/fcc_l_micro/EN.dat'
ignore
into table entities fields terminated by '|' ENCLOSED BY '' ESCAPED BY '' lines terminated by '\n'
(@record_type,@unique_system_identifier,@uls_file_number,@ebf_number,@call_sign,@entity_type,@licensee_id,@entity_name,@first_name,@mi,@last_name,@suffix,@phone,@fax,@email,@street_address,@city,@state,@zip_code,@po_box,@attention_line,@sgin,@frn,@applicant_type_code,@applicant_type_other,@status_code,@status_date,@lic_category_code,@linked_license_id,@linked_callsign) 
set
record_type = if(@record_type='', null, @record_type),
unique_system_identifier = if(@unique_system_identifier='', null, @unique_system_identifier),
uls_file_number = if(@uls_file_number='', null, @uls_file_number),
ebf_number = if(@ebf_number='', null, @ebf_number),
call_sign = if(@call_sign='', null, @call_sign),
entity_type = if(@entity_type='', null, @entity_type),
licensee_id = if(@licensee_id='', null, @licensee_id),
entity_name = if(@entity_name='', null, @entity_name),
first_name = if(@first_name='', null, @first_name),
mi = if(@mi='', null, @mi),
last_name = if(@last_name='', null, @last_name),
suffix = if(@suffix='', null, @suffix),
phone = if(@phone='', null, @phone),
fax = if(@fax='', null, @fax),
email = if(@email='', null, @email),
street_address = if(@street_address='', null, @street_address),
city = if(@city='', null, @city),
state = if(@state='', null, @state),
zip_code = if(@zip_code='', null, @zip_code),
po_box = if(@po_box='', null, @po_box),
attention_line = if(@attention_line='', null, @attention_line),
sgin = if(@sgin='', null, @sgin),
frn = if(@frn='', null, @frn),
applicant_type_code = if(@applicant_type_code='', null, @applicant_type_code),
applicant_type_other = if(@applicant_type_other='', null, @applicant_type_other),
status_code = if(@status_code='', null, @status_code),
status_date = if(@status_date='', null, @status_date),
lic_category_code = if(@lic_category_code='', null, @lic_category_code),
linked_license_id = if(@linked_license_id='', null, @linked_license_id),
linked_callsign = if(@linked_callsign='', null, @linked_callsign)
;
ALTER TABLE entities
DROP COLUMN uls_file_number,
DROP COLUMN ebf_number,
DROP COLUMN status_code,
DROP COLUMN status_date;