# Automated Data Processing Pipeline for Identification of High Frequency Trading Radio Infrastructure
## M.Chen, V. Gupta, J. Huang, A. Wang 

---
## Meet the Team
___

### Matthew Chen (mc52@illinois.edu)

Matthew is a current Senior at the University of Illinois at Urbana-Champaign majoring in Statistics + Computer Science who is graduating in December 2022. Matthew intends on pursuing a Masters in Science in Computer Science in the Fall of 2023.
Matthew has previously interned at Amazon and the University of Chicago and will be returning to Amazon upon graduation. Matthew is interested in academic research and enjoys learning about high-performance computing, deep learning applications, and 
bio-inspired computing. 

___
### Vanshika Gupta (vg18@illinois.edu)

Vanshika is a graduate student at the University of Illinois at Urbana-Champaign in the Grainger College of Engineering. Currently, she is trying to find the solution to a fundamental problem in Algorithmic Game Theory and Fair division that has been
unsolved since 2017 despite growing interest and research in this field. She has been unsuccessful so far. She will be graduating with a M.S. (Thesis) in May 2023.
___
### Jieshu (Jessica) Huang (jieshuh2@illinois.edu)

Jieshu is a current Senior at the University of Illinois at Urbana-Champaign majoring in Computer Science who will graudate in May 2023.
Jieshu has interned at Google and intend to pursue a master degree starting in the Fall of 2023.
___
### Allen Wang (allenhw2@illinois.edu)

Allen Wang is an undergraduate senior studying Computer Science at the University of Illinois at Urbana-Champaign in the Grainger College of Engineering. Allen is graduating December 2022 and is 
expected to return to his previous position as a full time Software Development Enginner at Amazon Web Services. Allen has experience working on cloud based backend systems that primarily run
on AWS. He previous exprience has seen him working on internal apis for B2B SaaS companies with fully enclosed data pipelines and internal security tools used at a high level.

___



## Project Description (abstract)

This is the final report for our semester-long project for IE421 - High-Frequency Trading Technology under [Professor David Lariviere](https://davidl.web.illinois.edu/). He is a very cool guy!!!!
 
The identification of high-frequency trading infrastructure has always been of interest in the field of communication networks. In a recent paper by [Bhattacherjee et. al](https://bdebopam.github.io/papers/imc2020-hft.pdf) in 2020, the authors identified many different high-frequency trading networks that linked data centers along the Chicago-New Jersey corridor. To identify these links, the authors parsed through the FCC Universal Licensing System (ULS) database and manually identified pairs of transmitter-receiver links based on their heuristics.
However, they could not associate the links with commercial entities because they did not have enough substantial evidence to make those associations. In this project, we attempt to solve this problem by creating an automated data processing pipeline that implements certain heuristics to
automatically identify high-frequency trading paths using the FCC Universal Licensing System. Furthermore, we identify paths between Chicago and Washington D.C. and from Washington D.C. to New Jersey as high-frequency trading companies are known to have paths on that route
as well to be able to transmit information released at the FED to their servers in New Jersey and Chicago as fast as possible. We also included millimetre wave data from [micronetcom.com](https://www.micronetcom.com/LinkRegistration/Query.aspx).
 
Our data processing pipeline is divided into three steps, each taking a subset of all microwave links registered under the FCC and refining the subset until there exists a subset that only contains paths related to HFTs. The first step is to create a subset of all microwave antennas that
are close to the shortest straight line distance between Chicago and New Jersey and between Chicago to DC and DC to NJ. As long as the registered antennas were within a certain distance threshold of the straight line distance between to suspected endpoints of a path, had a similar angle between the transmitter and receiver,
and were less than 15 KM between transmitter and receiver then we included it in the set. Next, we filtered based on the antennas license mailing/contact information, combining it with a list of [all registered SEC broker-dealers in the US](https://www.sec.gov/help/foiadocsbdfoia). We kept certain
links that had contact address information close to a registered SEC broker-dealer's mailing address based on a distance threshold as well. Finally, we loaded the remaining subset into a NetworkX graph and performed graph algorithms between data centers to get fully constructed paths.
 
Additionally, we were introduced to the research of Bob Van Valzah in the series titled "Shortwave Trading", [Part I of the series can be found here](https://sniperinmahwah.wordpress.com/2018/05/07/shortwave-trading-part-i-the-west-chicago-tower-mystery/). The premise is simple,
Bob Van Valzah discovered a mysterious antenna tower that seemed to contain both antennas for shortwave radio and microwave transmission. He did some investigating and realized that it the microwave link was owned by IMC Trading and that the shortwave antenna could be used for communication with firms
overseas in Europe. This led to the shortwave radio analysis of our project, in which we attempt to pinpoint shortwave-microwave combinations like the one Bob Van Valzah found in his research.


### Technologies
___
#### Tech Stack for Dev Ops
- __Vagrant__:
	We utilized vagrant to provision or virtual machine so that our project can be easily transferable and the process of setting up the virtual machine and all of our backend provisioning scripts could be automated.
- __Virtual Machines__:
	We compartamentalize our entire project in a Virtual Machine so that it can be run on any computer with a simple provisioning script.
- __Git Lab__:
	We used Git/GitLab to host and version control our project because Gitlab has a lot of CI/CD features and more features such as dashboards, an IDE, etc. that other competitors like Github do not have.
- __Fedora__:
	Fedora is the operating system we use in our Virtual Machine. It is extremely lightweight, which helps boot our project extremely fast.
#### Tech stack for Backend Systems
- __Apache__:
	We use Apache for Network Hosting to host our project on a webserver. The Apache webserver queries a python script to generate a kml file whenever it is requested by a client.
- __MySQL__:
	We store all of our data in a MySQL database. MySQL is especially powerful because of its well-tested reliability and speed and has enabled us to parse and process large FCC databases with ease. Furthermore, the 
  FCC's database files are already in a relational schema, so it was very easy to import their data to our project using MySQL.
- __Python__:
	Python is the main programming language we used in our data pipeline. We used various geospatial and data processing libraries like GeoPy and Pandas in order to manipulate data within our MySQL server. Although slower than languages 
  like C++, its easy-to-read syntax enables use to rapidly experiment and develop new heuristics for our pipeline.
- __KML / Google Earth / SimpleKML__:
	KML or, Keyhole Markup Language, is a file format supported by Google that enables the displaying of geographic information on a map. Google Earth supports KML files and enables us to visualize our discoveries. Simple KML is a 
  Python Library to enable us to quickly create a KML file.
- __Google Map API__:
	The Google Map API solved a vital problem that stemmed from the FCC's database. Since there is no standardized way to enter in an address there were many variations (IE "123 E 27th St apt 1" and "123 EAST 27th Street, Apartment 1"). We needed to normalize the address data, and the Google Maps API was the only reliable way.
  We attempted the usage of other normalization services, but they were either behind a massive paywall, very unreliable, or unfit to handle the sheer amount of processing we needed.
- __Network X__:
	NetworkX is a graph library in Python that helps us resolve graph related queries and easily identify paths. Since MySQL is not very performant in terms of performing graph traversals, we load the MySQL into a NetworkX graph at the last 
  step in to perform various graph algorithms with ease on the data.
- __Linux Bash Scripting__:
	To provision the virtual machine and perform the pipeline work automatically, we used shell scripts that automatically ran certain files at certain times to create intermediate results and to simply allow a user to watch and let the magic happen.
- ~~the ability to type vagrant up and vagrant reload a lot~~

### Components
Our high level view of our pipeline can be seen in this graph from start to finish.

![Flowchart](https://cdn.discordapp.com/attachments/1010432397839437945/1051837471098417224/Path_Analysis_Flow_Chart.png)

### Git Repo Layout 

```
Automated Data Processing Pipeline for Identification of High Frequency Trading Radio Infrastructure
├── data
├── google-earth-server
│   └──cgi-bin
├── google_maps_SEC
│   ├── data
│   ├── notebook
│   ├── test
│   └── src
├── provision_files
├── provision_scripts
├── shortwave_data
├── sql
└── wave_to_radio
```

To update this data, run:
```
./generate_entity_company.sh
```

## Instructions for using our project

To use our project make sure to download Google Earth Pro on your computer beforehand: https://www.google.com/earth/about/versions/. Next clone our repository:

```
git clone https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2022/ie421_hft_fall_2022_group_05/professors_snippets_for_fcc_team.git
cd professors_snippets_for_fcc_team
vagrant up
```

All the magic happens in `vagrant up` and it takes a long time to run and displays the entire process. Depending on whether or not you have downloaded the FCC databases before, our project will automatically download the most recent data 
from the FCC and the SEC. Once that is setup all you have to do is open up a browser and head to:

```
http://127.0.0.1:8000/cgi-bin/main.py
```

Once you reach this page a file will be automatically downloaded called `main.py`. This is not a Python file. It is a KML file so rename/change its extension to `main.kml`. Afterwards, open the file in Google Earth Pro and visualize our output.

## Testing 
Our project does not have Unit Tests that check for code coverage or anything of the sorts. However we do utilize simple tests to ensure that all of our required packages get installed when provisioning our virtual machine. This is fully automatic and runs during provisioning; if the test detects missing packages, it blocks further movement.

## Project Results

### Modelling the network as a graph

It is important to understand how the nodes are modelled into the graph as an object. As illustrated in this diagram, we cannot uniquely determine a node with the callsign as the receiver is the same callsign when it acts as a receiever, but when the same receiver acts a transmitter, it has a different callsign. According to our findings, there is no way to determine the callsign of the receiver from the transmitters callsign or record. After quite some rigourous finding, we establish that the **owner** email of an antenna (under which the transmitter is registered in the FCC database) is the same as the receiver antenna's email (when it acts as a transmitter). Hence, we use *latitude + longitde + email* to uniquely identify a node. The node object also contains other relevant information such as transmitter height and possible company name.  

Next, we create edges between nodes and set their weight equal to the geographic distance between the the source and destination node (using appropriate GIS measuring system). This completed the graph modelling.  
![node](report_images/node_diagram.png)

Next, based on the data center locations and assuming some error search radius. we create sets of points close to each dataacenter. We have 9 data centers including the Toronto datacenter and FED. So we get 9 sets and ultimately, we will be finding paths from points in set A to points in set B, where A and B are two arbitrary data centers.  

We operate uUnder the assumtion that the HFTs will try to have paths as close as possible to the practically shortest possible path connecting two data centers. Here, the term practically possible **shortest path** is used to denote the path (network of connected antennas) with the shortest length. Since the straight line between A and B cannot always be achieved owing to natural obstructions, our reference measure is much more robust. After finding the shortest path between A and B, we only include paths that have lengths differing by only some amount, beta, from the shortest path length.

*length_of_valid_path_b/w_A_B - shortest_path_length_b/w_A_B > beta*

where

*beta = α * distance between data centers A and B*
  
After generating all paths, we compute path latencies and associate a path with a company and lastly, generate all the outputs necessary to generate and label the kml file displayed below and also present in /google_maps_SEC/data/networkX_results/. 


### Screenshots and Example Outputs
___
#### Identified Paths from IL to NJ from our process
<p align="center">
  <img src="https://cdn.discordapp.com/attachments/1010432397839437945/1048797115020738661/image.png" alt="Vanshika Paths 1"/>
</p>

<p align="center">
  <img src="https://cdn.discordapp.com/attachments/1010432397839437945/1051836991068704808/image.png" alt="Vanshika Paths 2"/>
</p>

#### The tables from this process

![Table1_ex](https://cdn.discordapp.com/attachments/1010432397839437945/1051838911766335550/image.png)

![Table2_ex](https://cdn.discordapp.com/attachments/1010432397839437945/1051839167237201960/image.png)


#### Results for Microwave and Millimitre paths

  **Left column** : source datacenter (A) - destination datacenter (B)\
  **Shortest Path (in miles)** : length of the shortest network path of antennas from A to B\
  **Min # Links (hops)** : minimum number of antennas connecting from A to B\

  Microwave\
  ![output1](report_images/microwave_results.png)

  Millimetre\
  ![output2](report_images/millimetre_results.png)

___
#### Tables created during our process
<p align="center">
  <img src="https://cdn.discordapp.com/attachments/1010432397839437945/1051837348373078066/image.png" alt="Our Table Setup"/>
</p>

___
#### Final view of our findings
![Colored_Paths_All](https://cdn.discordapp.com/attachments/1010432397839437945/1051838047915888670/image.png)

___
#### Shortwave - Microwave Link identification
Bob Van Valzah's Original Antenna Finding
<p align="center">
  <img src="https://cdn.discordapp.com/attachments/1010432397839437945/1051838147920666654/fccpathmapannotated.png" alt="Bob Van Valzah's Original Antenna Finding"/>
</p>
Our identified Microwave to Shortwave Pair for his finding.
<p align="center">
  <img src="https://cdn.discordapp.com/attachments/1010432397839437945/1051833896095989841/image.png" alt="Our identified Microwave to Shortwave Pair"/>
</p>

## Postmortem Summary

1. __Matthew__:
	  1. __Individual Contributions__:
  	As the team leader of my group, I managed each of my groupmates in team meetings and assigned various tasks amongst them to complete this project. To begin with, I started the project by first conducting personal research on how the 
    FCC stores its data and what is registered as an entity in their Universal Licensing System. Next, after the professor provided us with some starter code, I edited all of the provisioning scripts, added libaries like SimpleKML to make
    KML generation easier, and cleaned up various table schema loaded into the MySQL server. I then decided to investigate how to identify high frequency trading links based on the information provided in these databases. One 
    of the first approaches I tried was to automatically identify key data centers in the equities world by finding hotspots in antennas. This led down to a rabbit hole of learning several different Geospatial analysis techniques and Geographic
    Information Systems as I tried to use kernel density estimation to find the hotspots. Although I was successful in identifying the CME and NASDAQ data centers using this method, it failed to find other datacenters closer to more urban areas
    like the Mahwah datacenter and the NYSE datacenterin Secacaus, New Jersey. After getting the actual coordinates of the major equity datacenters in North America, I decided that identifying HFT links required several conditions to be true
    and the exact numerical parameters for this would be heuristically defined. This is where I assigned each group member a step in the data processing pipeline. I personally contributed to the beginning of the data processing pipeline in 
    which I calculated the straight line joining each data center and then calculated the angle and distance of all antennas near this path. If the distance and angle meet a certain threshold then they will be added to my list. 
    Afterwards, I also checked if the line between the transmitter and reciever was less than 150 KM because in the paper usually microwave links are spaced 100-110 KM apart. This is a very coarse skim of the dataset which will be
    further refined. Finally, I also worked on writing the Python script to generate the KML file for the Apache Web Server.

    I also wrote a database normalization wrapper for the FCC Schema since it has redundant schema with unit tests but we never used it.  
  
    2. __What did I learn?__:
    Before this project, I had very little experience with using virtual machines and hardware virtualization. I also knew very little about how radio communication worked and why it is so popular in High Frequency Trading. I primarily  learned a lot about using automation to make tasks in my project simpler and also how to properly provision a virtual machine. Moreover, I had never used SQL before so I learned a lot about MySQL and how to query tables using  MySQL. Finally, I learned a lot about geographic information systems, the history behind different representations of the earth, geodesics, and various Python libraries related to these fields.
  
    3. __If I had a time machine and could go back to the beginning, what would I have done differently?__:
    I believe my biggest regret was not asking the professor for help earlier. The professor was very insightful in providing us a direction and even sample code to get us started after our group spent a few weeks confused on where to begin.
		The professor was always available to discuss about the project and answered any of our questions if we messaged him on our class discussion board. Without his guidance, we would have never been able to make the project we made today.

      4. __Future Extensions__:
    A possible future extension for this work is to host this virtual machine persistently on a cloud provider like AWS so that it can act like a web-application. Then the application could constantly rerun data downloading and update its visualizations based on new postings from the FCC and SEC. Additionally, it would also be cool to identify links between Canada's TMX and some of the US's datacenters because we could not really find any in our FCC data/ 
  
    5. __Advice to Future Students__:
    The professor is your friend. Don't be afraid to ask him questions, even if they might seem simple. He is always available and he will always help. No matter how lost you might feel, he can help.
  
  
2. __Vanshika__:

    1. __Individual Contributions__:

        My primary contribution to the project was in coming up with the idea of designing and subsequently implementing the network graph based approach for finding the HFT links for microwave and millimetre wave data. This process required quite a lot of revisions to optimize associated computational complexity of the task and the large data size and to be able to run the entire procedure in a reasonable amount of time. I was able to reduce the time for this from almost an hour to 10-12 minutes for the microwave data. 

        Initially, we were not sure that the idea would work as the locations and other information was not very accurate. However, after writing the code's first draft, it gave good results and we went full in the method. As the callsigns were not unique and nothing seemed indicative of one to one mapping of path to company, I examined the other fields in the database related to paths to better model the graph structure. The final output from this procedure is the set of first 6 results in the report with detailed information of each path. The input to this procedure comes from Matt's filtered results and at the end. I associate one company name to each path using Jieshu's processing. I have described the details in the "Project results" section of the report.

        Additionally, after Matt's approach failed to automatically identify the data centers, I attempted to identify them by computing the degree of nodes, however, this also did not work.
    
        Before taking up this part, I spent some time understanding the paper that the prof shared (BirdEye view) and worked on setting up the SQL server/workbench for testing SQL queries on the database. During this, I learned different types of database normalization tecnhiques as well. After setting up the vagrant and encountering a lot of issues, I decided to write basic tests for checking various installations such as SQL, apache and data downloads from FCC database. 

        PS: commit username: both @vansjyo and @vg18

    2. __What did I learn?__:
    
        I learned vagrant and how to writing tests. I had not written code flows like this before. I also learnt a lot about how HFT communications work and how sneaky they are.

    3. __If I had a time machine and could go back to the beginning, what would I have done differently?__:

        I would have spent much more time investigating the database and earlier.
    
    4. __Future Extensions__:
        * Optimize the code to its best speed. I feel the sql queries and python scripts do not use the most optimized data structures and methodologies.
        * Try to compe up with a method to be able to identify the data centers automatically or determininstically prove that it is not possible.

    5. __Advice to Future Students__:
        * Ask questions. That's the only way you can learn anything in this class. The professor is a know-it-all and his knowledge and willingness to help might blow your mind at times.
        * Plan your courses well. This ptoject will require time and if you cannot dedicate that, drop the class.
        * Do the Bloomberg market concepts course as early as possible.
        * DO not underestimate the time you will need just to get a hang of the overall project setup requirements.

      
  
3. __Jieshu__:
    1. __Individual Contributions__:
  	My job is to find which broker-dealers use which HFT links.
    At first, I worked on addressing normalization. The purpose of that was that we would be able to use it as a foreign key in finding the paths. I tried a couple of approaches and found that using google API was the easiest way. However, scanning through every row of every table and sending a query to Google Map API was a nightmare. (Google map refuses to a patch mode?) 
    I found that company names registered in the FCC table were usually the name of the wire service company we didn't want.
    To find the radio wave paths that each broker-dealer uses, I tried two approaches.
       1. Assuming that broker-dealers would use antennas around their headquarters, I tried to find those antennas and use these as start points of the paths.
      		The problem with this approach was the size of the data. Even with multi-threading, the matching still took too long. 
          Also, Multiple broker-dealers in the same building will lead to a false positive result.
          I also noticed that MySQL allowed a maximum of 100 connections and increasing the number of connections would significantly delay the transaction time of individual transactions.
       2. Notice that there exists a contact mailing address in the entity table. I tried to match that to the broker-dealers' headquarters addresses in the SEC table base on distance.
       		This approach captured a limited amount of broker-dealers.
        	For example, 
          It worked well with Jump Trading and we could see a clear pattern between major data centers in Chicago, NewYork, and New Jersey.
          I could capture paths from IMC (Those paths either have emails with @imc or has exact same mailing address as IMC headquarters). But we found that 
          these paths lead to very weird destinations. I assumed that IMC doesn't register those paths between data centers under their own email address.
          I could not capture any path for DRW. They may register them under other offices or law firms.
          Also, some links are managed by wire companies and I cannot find information on broker-dealers in the registration information.

  2. __What did I learn?__:
  		The major takeaway for me is the ability of mining information from data. My goal is clear but how to achieve it is very obscure. I have to use uncertain heuristic assumptions, 
      test that and see if it works in practice. 
  
  3. __If I had a time machine and could go back to the beginning, what would I have done differently?__:
  		Looking back, I was overly reliant on GPS data. Since I started with address normalization and found google map API the easiest approach, I tend to use GPS data to solve problems.
      I might ignore other information in the data that might also be useful in identifying path company match.
      
  4. __Future Extensions__:
  		I still think solution 1 is worth exploring. With caching and more understanding of SQL, I will be able to solve the problem of time. Those companies can hide their information in the registration,
      but as long as they use these networks, we will be able to capture that. 
  
  5. __Advice to Future Students__:
  		Preview for lectures. The professor will go through each concept very fast and if you don't know that in advance, you quite easily lost



4. __Allen__

    1. __Individual Contributions__:
      Since the other members of my group were working on the direct path analysis pipeline, I assumed the "odd-job" role. This usually entailed doing explorative work to find what we can do to add to or improve our system. A recollection of my contributions being with a dev ops type role of setting up AWS for group development. We initially had intentions of hosting our entire project on the cloud so we could directly ssh into an EC2 instance and host our project on a website without needing an active server. This idea was cut in preference of using vagrant and virtual box for ease of use. I think ideally, it would still be beneficial to shit towards serverless because then you wouldn't need to set up your own personal server or etc. Afterward, I was tasked with aggregating and parsing the millimeter wave data from MicroNet and setting up the tables. This part also involved updating and adding them to our KML; however, at this point, I noticed that our current format was very cumbersome and difficult to look at, I then reorganized our KML structure to better handle different entities and different wave types, ultimately it was changed again and unused, but the core ideas were there. From there my next task was to web-scrape the combination of the fcc entity name and sec broker for every pairing. Given the obscene amount of google search pings, I did research into the viability. Long story short: Google puts web scraping of this type behind a massive paywall, and the allocated free amount of queries was not even close to the amount necessary. At this point, I was left in a limbo state while we tried to brainstorm other ways of assisting with the path-finding aspect of the project. In the meantime, I explored using an alternative geolocating service besides Google Maps API (due to the Google Paywall, we are unable to geolocate every time the machine is booted and thus we need cached data) and ran experiments to check the validity (through manual verification), but all the options were either paid services or incredibly unreliable. I additionally explored the concept of hosting a webpage with all of our findings as a way to bypass the need for Google Earth on the user's local machine: this did not bear fruit but could be explored in the future along with hosting on the cloud. If we were to design our own webpage for this, we wouldn't be limited by the restrictions of KML and could have more flexible information displays. Finally, I was tasked with implementing the findings of Bob Van Valzah and trying to find shortwave antennas that might currently be utilized by High-Frequency Firms. Since I was following his findings in the original article I noticed that he used too many assumptions or information that we couldn't readily obtain. My next step was to try to develop a different algorithm that performed the same results; this would lead to large amounts of searching through the FCC database for anything that could be exploited to do this calculation. The process I thought would be valid would be to use the azimuth that the FCC provided for every antenna and calculate whether or not it was within the bounds of following the shortest path to international exchange centers (like the London or Tokyo exchanges). This involved research that lead me into the interesting nooks and crannies of geospatial analysis with code. I implemented a base algorithm and parsed through the data, but there was something wrong, the azimuth provided by the FCC simply made no sense. I was unable to create the number they provided through all sorts of different calculations. This was dropped and the next plan of action would be to attempt to locate antennas that were in low-density areas that were also near a radio tower. Long story short, after attempting to find the original antenna identified by Bob Van Valzah, I was unable to find it. During this time of trying to make it work, I discovered ways that we could improve the speeds and results of queries; I initially discovered that I was getting a lot of redundant results, and then this led down a rabbit hole of analyzing the databases provided from the FCC to see if there was any way to reduce the number of joins. I also changed how we query certain connections and made sure we join on more than one category for some tables or else we would have an obscene amount of redundant and technically incorrect results. After this, I did more digging and came across Bob's PART IV, an article I did not know existed. This had the key to my shortwave problem; there was a whole other database for shortwave antennas. From this point it was easy, I did a series of queries on filtered data (for speed) and combined antennas that were colocated. At this point, I was finally able to find the original tower that Bob Van Valzah identified on his fateful bike ride.

        Tl:Dr
          1. ~~AWS implementation for dev ops~~
          2. Millimetre Wave data parsing
          3. ~~Web Scraping to create a database of entity to broker pairing~~
          4. ~~alternative geolocating~~
          5. ~~Alernative ways to display findings/Web page hosting for findings~~
          6. ~~Microwave to Shortwave using azimuth~~
          7. ~~Microwave to Shortwave using low density and no outward path method~~
          8. Reduced redudancy in our queries by analyzing ways we could reduce the query amount
          9. Reorganized our provisioning system to better identify where we are encountering failures
          10. Parsed OET experimental data and loaded into our system
          11. Used parsed OET table (and other filtered tables) to identify Shortwave-Microwave pairs

      2. __What did I learn?__:
      Through this lengthy and challenging process, I learned a great deal about the field of geospatial analysis. All I knew about this previously was that it existed; after just scratching the surface it has made me appreciate every map I see. There are so many factors and variables that inter-disciplinary knowledge to integrate. Additionally, I learned about dev ops setups, database manipulation, data mining, the inner workings of high-frequency trading, and so much more throughout this process. However, I think the biggest takeaway from this project is just the experience of taking a conceptual idea and bringing it to fruition with little to no guidelines in place to lead you (minus pinging the professor of course), but yes the concept we started with was something that took me a long time to grasp.

      3. __If I had a time machine and could go back to the beginning, what would I have done differently?__:
      The biggest thing I would have liked to have done differently is to understand that long and arduous research is less efficient than smart research. When I began working on implementing Bob's findings, I quickly discovered that he had access to information that we couldn't have: the visual confirmation of the antenna's existence. I then had to research, experiment, and implement all sorts of hacky ways to try and replicate the results. Although I learned a lot from each iteration of my approach, none of them were fruitful. It took me way too long to discover that my assumption that Bob published a "three-part series" was incorrect; it was a "four-part series". It was in the fourth part that he clearly outlined how to find these suspects. The biggest thing that actually skyrocketed my progression was the fact that there was another table! One that contained all of the shortwave antennas. I felt so inept and relevant at this discovery, and I was able to implement my version of his process. I was finally able to find the original antenna in Bob's article. It would have been nice to find the findings earlier so that I could have either improved the findings or contributed in another way to the core project. Overall, I am still proud of the work we were all able to accomplish.

      4. __Future Extensions__:
      As I identified above, I really would have liked to have run this on the cloud so that it can be easily run, maintained, and accessed. In conjunction with the cloud, it would be nice to design a whole page that would display our findings and allow the user to selectively filter by more categories. For instance, if we wanted the user to be able to get all of the millimeter connections for specific entities within a specific area. Additionally, if we could get access to the same data for countries like the UK or Tokyo, we could run the same process as we have done here in the states. Then I would also be able maybe to confirm whether or not a shortwave antenna is being used for international trade instead of speculating that it is.

      5. __Advice to Future Students__:
      This class is very loose. I say this in the sense that there is no real structure (except for a few attributes) to the class. If you believe you can learn in an environment without everything planned out (ie. a clear-cut assignment that gives you a specific question and a set way to answer it) this class might be for you (if you have an interest in high-frequency trading that is). I would also like to say that this class is very focused on its topic. If you wanted to take this class assuming it would be along the lines of an intro course, I would say that you're better off taking a finance or econ class. The Professor's knowledge and experience are very vast and he does not shy away from spreading the information he has accumulated. The professor isn't joking when he talks about whether or not you should take this class on day one. I did not take the warnings and I challenged the gauntlet of IE 421, did I pass? We shall see. Did a learn a lot about a topic that I was only originally only semi-interested in? A ton and I became much more interested in the topic. Does the professor instill in you the dread that the economy might crash and that your job is not as stable as you thought? yes.


# Link to video

https://drive.google.com/file/d/1zXNACPW6t_8b1K3yoEqK8mRiOtHeEfuk/view?usp=sharing
