import simplekml
import math


def num_to_rgb(val, max_val=3):
    i = (val * 255 / max_val);
    r = round(math.sin(0.024 * i + 0) * 127 + 128);
    g = round(math.sin(0.024 * i + 2) * 127 + 128);
    b = round(math.sin(0.024 * i + 4) * 127 + 128);
    return (r,g,b)

def toKMLdate(date_str):
    dates = date_str.split("/")
    month = dates[0]
    day = dates[1]
    year = dates[2]
    return "-".join([year, month, day])

def getAddress(entity_name):
    if entity_name != None and "@" in entity_name:
        address = entity_name.split("@", 1)[1]
        match address:
            case "mckay-brothers.com":
                return "mckay-brothers.com"
            case "wcwtech.com":
                return "wcwtech.com"
            case "geodesicnetworks.com":
                return "geodesicnetworks.com"
            case 'auburndata.com':
                return 'auburndata.com'
            case 'abservicesllc.com':
                return 'abservicesllc.com'
            case 'NeXXComwireless.com':
                return 'NeXXComwireless.com'
            case 'isignalnetworks.com':
                return 'isignalnetworks.com'
            case 'anova-tech.com':
                return 'anova-tech.com'
            case 'infiniumcm.com':
                return 'infiniumcm.com'
            case 'tatora.com':
                return 'tatora.com'
            case 'midwestics.com':
                return 'midwestics.com'
            case 'apsaranetworks.com':
                return  'apsaranetworks.com'
            case 'bsonetwork.com':
                return 'bsonetwork.com'
            case 'striketechnologies.com':
                return 'striketechnologies.com'
            case 'akingump.com':
                return  'akingump.com'
            case 'surveillancetechs.com':
                return 'surveillancetechs.com'
            case 'bobbroadband.com':
                return 'bobbroadband.com'
            case 'gmail.com':
                return 'gmail.com'
            case 'newlinenet.com':
                return 'newlinenet.com'
            case default:
                return 'unrecognized'
    return 'unrecognized'

def color_based_on_path_number(path_number, max_paths):
    r,g,b = num_to_rgb(path_number, max_paths)
    return simplekml.Color.rgb(r,g,b)

def determineStyle(entity_name):
    if entity_name != None and "@" in entity_name:
        address = entity_name.split("@", 1)[1]

        match address:
            case "mckay-brothers.com":
                return simplekml.Color.antiquewhite, 1
            case "wcwtech.com":
                return simplekml.Color.blue, 1
            case "geodesicnetworks.com":
                return simplekml.Color.red, 1
            case 'auburndata.com':
                return simplekml.Color.yellow, 1
            case 'abservicesllc.com':
                return simplekml.Color.green, 1
            case 'NeXXComwireless.com':
                return simplekml.Color.brown, 1
            case 'isignalnetworks.com':
                return simplekml.Color.cyan, 1
            case 'anova-tech.com':
                return simplekml.Color.darkgoldenrod, 1
            case 'infiniumcm.com':
                return simplekml.Color.darksalmon, 1
            case 'tatora.com':
                return simplekml.Color.darkseagreen, 1
            case 'midwestics.com':
                return simplekml.Color.indigo, 1
            case 'apsaranetworks.com':
                return simplekml.Color.lemonchiffon, 1
            case 'bsonetwork.com':
                return simplekml.Color.khaki, 1
            case 'striketechnologies.com':
                return simplekml.Color.gray, 1
            case 'akingump.com':
                return simplekml.Color.greenyellow, 1
            case 'surveillancetechs.com':
                return simplekml.Color.lightcoral, 1
            case 'bobbroadband.com':
                return simplekml.Color.orange, 1
            case 'gmail.com':
                return simplekml.Color.snow, 1
            case 'newlinenet.com':
                return simplekml.Color.silver, 1
            case 'weblineholdings.com':
                return simplekml.Color.darkmagenta, 1
            case 'highland-systems.com':
                return simplekml.Color.olive, 1
            case 'wacorp.net':
                return simplekml.Color.goldenrod, 1
            case 'scientelsolutions.com':
                return simplekml.Color.gainsboro, 1
            case 'duanemorris.com':
                return simplekml.Color.chocolate, 1
            case 'RADYN.com':
                return simplekml.Color.hotpink, 1
            case 'cellanet.co.ul':
                return simplekml.Color.violet, 1
            case 'teza.com':
                return simplekml.Color.tan, 1
            case default:
                return simplekml.Color.black, 1

