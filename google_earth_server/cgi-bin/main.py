#!/usr/bin/python
import pymysql.cursors
import simplekml
from main_helper import *

def populateKML_with_millimetrewave_paths(kml, cursor, folders):
    pre_sql = """
        SELECT count(*) as cunt
        FROM millimetrewave_finalTable2 
    """
    cursor.execute(pre_sql)
    results = cursor.fetchall()
    max_paths = 0
    for result in results:
        max_paths = result['cunt']

    sql = """
            select
                ft1.path_number as pn,
                ft1.call_sign as cs,
                t_lat as lat_decimal,
                t_lon as lon_decimal,
                r_lat as r_lat_decimal,
                r_lon as r_lon_decimal,
				t_height as t_height,
                r_height as r_height,
                ft1.email as email,
                mlh.radio_service_code as status,
                mlh.expired_date as expired_date,
                mlh.cancellation_date as cancellation_date,
                mlh.effective_date as effective_date
                from 
                    millimetrewave_finalTable1 ft1 NATURAL JOIN millimetrewave_finalTable2 ft2 JOIN microwave_license_header mlh ON (ft1.call_sign = mlh.call_sign)
        """

    cursor.execute(sql)
    results = cursor.fetchall()


    for result in results:
        lat_dec = result['lat_decimal']
        lon_dec = result['lon_decimal'] # * -1
        
        r_lat_dec = result['r_lat_decimal']
        r_lon_dec = result['r_lon_decimal']
        
        t_ground_elevation = 0
        r_ground_elevation = 0
        
        try:
            t_ground_elevation = float(result['t_height']) #
            r_ground_elevation = float(result['r_height']) # elevation is stored in meters
        except Exception as e:
            pass

        entity = getAddress(result['email'])
            
        if entity not in folders:
            entityFolders = []
            fol = kml.newfolder(name=entity)
            entityFolders.append(fol)
            millimetrefol = fol.newfolder(name="millimetre wave")
            entityFolders.append(millimetrefol)
            microwavefol = fol.newfolder(name="microwave")
            entityFolders.append(microwavefol)
            folders[entity] = entityFolders

        fol = folders[entity][1]
        line_string = fol.newlinestring(name=result['email'], coords=[(lon_dec, lat_dec, t_ground_elevation), (r_lon_dec, r_lat_dec, r_ground_elevation)])
        if result['effective_date'] != None:
            line_string.timespan.begin = toKMLdate(result['effective_date'])
        if result['status'] == 'C' and result['cancellation_date'] != None:
            line_string.timespan.end = toKMLdate(result['cancellation_date'])
        elif result['status'] == 'E' and result['expired_date'] != None:
            line_string.timespan.end = toKMLdate(result['expired_date'])

        color = color_based_on_path_number(result['pn'], max_paths)
        line_string.style.linestyle.color = color

    return kml

def populateKML_with_microwave_paths(kml, cursor, folders):
    pre_sql = """
        SELECT count(*) as cunt
        FROM microwave_finalTable2 
    """
    cursor.execute(pre_sql)
    results = cursor.fetchall()
    max_paths = 0
    for result in results:
        max_paths = result['cunt']


    sql = """
            select
                ft1.path_number as pn,
                ft1.call_sign as cs,
                t_lat as lat_decimal,
                t_lon as lon_decimal,
                r_lat as r_lat_decimal,
                r_lon as r_lon_decimal,
				t_height as t_height,
                r_height as r_height,
                ft1.email as email,
                mlh.radio_service_code as status,
                mlh.expired_date as expired_date,
                mlh.cancellation_date as cancellation_date,
                mlh.effective_date as effective_date
                from 
                    microwave_finalTable1 ft1 NATURAL JOIN microwave_finalTable2 ft2 JOIN microwave_license_header mlh ON (ft1.call_sign = mlh.call_sign)
        """


    cursor.execute(sql)
    results = cursor.fetchall()

    for result in results:
        lat_dec = result['lat_decimal']
        lon_dec = result['lon_decimal'] # * -1
        
        r_lat_dec = result['r_lat_decimal']
        r_lon_dec = result['r_lon_decimal']
        
        t_ground_elevation = 0
        r_ground_elevation = 0

        try:
            t_ground_elevation = float(result['t_height']) #
            r_ground_elevation = float(result['r_height']) # elevation is stored in meters
        except Exception as e:
            pass

        entity = getAddress(result['email'])

        if entity not in folders:
            entityFolders = []
            fol = kml.newfolder(name=entity)
            entityFolders.append(fol)
            millimetrefol = fol.newfolder(name="millimetre wave")
            entityFolders.append(millimetrefol)
            microwavefol = fol.newfolder(name="microwave")
            entityFolders.append(microwavefol)
            folders[entity] = entityFolders
        fol = folders[entity][2]

        line_string = fol.newlinestring(name=result['email'], coords=[(lon_dec, lat_dec, t_ground_elevation), (r_lon_dec, r_lat_dec, r_ground_elevation)])
        
        if result['effective_date'] != None:
            line_string.timespan.begin = toKMLdate(result['effective_date'])
        if result['status'] == 'C' and result['cancellation_date'] != None:
            line_string.timespan.end = toKMLdate(result['cancellation_date'])
        elif result['status'] == 'E' and result['expired_date'] != None:
            line_string.timespan.end = toKMLdate(result['expired_date'])

        color = color_based_on_path_number(result['pn'], max_paths)

        line_string.style.linestyle.color = color
        line_string.altitudemode = simplekml.AltitudeMode.relativetoground
        line_string.extrude = 1
    return kml

def populateKML_with_broker_dealers(kml, cursor):
    sql = """
        select
            company_name as name,
            latitude as latitude,
            longtitude as longitude
            FROM bds_location
        """

    cursor.execute(sql)
    results = cursor.fetchall()
    
    for result in results:
        lat = result['latitude']
        long = result['longitude']


        if lat != None and long != None:
            point = kml.newpoint()
            point.coords = [(long, lat)]
    return kml


def populateKML_shortwave_microwave_pairs(kml, cursor):
    sql = """
		select
            sap.fcc_entity_name as fcc_name,
            sap.fcc_lon_decimal as m_lon,
			sap.fcc_lat_decimal as m_lat,
			sap.oet_entity_name as oet_name,
			sap.oet_lon_decimal as s_lon,
			sap.oet_lat_decimal as s_lat,
            ec.company_name,
            ec.distance,
			mlh.radio_service_code as status,
			mlh.expired_date as expired_date,
			mlh.cancellation_date as cancellation_date,
			mlh.effective_date as effective_date
            FROM shortwave_antenna_pairs sap LEFT JOIN filtered_entity_company ec ON (sap.fcc_callsign = ec.call_sign) JOIN microwave_license_header mlh ON (mlh.call_sign = sap.fcc_callsign)
		 """

    cursor.execute(sql)
    results = cursor.fetchall()
    
    for result in results:
        fcc_entity_name = result['fcc_name']
        fcc_lon_decimal = result['m_lon']
        fcc_lat_decimal = result['m_lat']
        oet_entity_name = result['oet_name']
        oet_lon_decimal = result['s_lon']
        oet_lat_decimal = result['s_lat']

        full_name = "-".join([fcc_entity_name, oet_entity_name])

        linestring = kml.newlinestring(name=full_name)
        linestring.coords = [(fcc_lon_decimal, fcc_lat_decimal, 50),
                                (oet_lon_decimal, oet_lat_decimal, 50)]
        linestring.color = simplekml.Color.white
        linestring.altitudemode = simplekml.AltitudeMode.relativetoground
        linestring.extrude = 1
        linestring.style.linestyle.width = 10
        if result['effective_date'] != None:
            linestring.timespan.begin = toKMLdate(result['effective_date'])
        if result['status'] == 'C' and result['cancellation_date'] != None:
            linestring.timespan.end = toKMLdate(result['cancellation_date'])
        elif result['status'] == 'E' and result['expired_date'] != None:
            linestring.timespan.end = toKMLdate(result['expired_date'])

    return kml

try:

    connection = pymysql.connect(host='127.0.0.1',
                                     port=3306,
                                     user='root',
                                     password='vagrant',
                                     db='python_fcc_db_test',
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)
    kml = simplekml.Kml()
    kml.document.name = "data"
    print("Results: <ul>")
    print ("Content-Type: application/vnd.google-earth.kml+xml\n")

    folders = {}
    with connection.cursor() as cursor:
        kml = populateKML_with_millimetrewave_paths(kml, cursor, folders)
        kml = populateKML_with_microwave_paths(kml, cursor, folders)
        kml = populateKML_shortwave_microwave_pairs(kml, cursor)
        #kml = populateKML_with_broker_dealers(kml, cursor)
        print(kml.kml())
        
except Exception as error:
    print(error)
finally:
    connection.close()



