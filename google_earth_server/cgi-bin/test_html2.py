#!/usr/bin/python
import pymysql.cursors
import simplekml
import math

def num_to_rgb(val, max_val=3):
    i = (val * 255 / max_val);
    r = round(math.sin(0.024 * i + 0) * 127 + 128);
    g = round(math.sin(0.024 * i + 2) * 127 + 128);
    b = round(math.sin(0.024 * i + 4) * 127 + 128);
    return (r,g,b)

def toKMLdate(date_str):
    dates = date_str.split("/")
    month = dates[0]
    day = dates[1]
    year = dates[2]
    return "-".join([year, month, day])
'''
def determineStyle(entity_name):
    if entity_name != None and "@" in entity_name:
        address = entity_name.split("@", 1)[1]

        match address:
            case "mckay-brothers.com":
                return simplekml.Color.antiquewhite, 1
            case "wcwtech.com":
                return simplekml.Color.blue, 1
            case "geodesicnetworks.com":
                return simplekml.Color.red, 1
            case 'auburndata.com':
                return simplekml.Color.yellow, 1
            case 'abservicesllc.com':
                return simplekml.Color.green, 1
            case 'NeXXComwireless.com':
                return simplekml.Color.brown, 1
            case 'isignalnetworks.com':
                return simplekml.Color.cyan, 1
            case 'anova-tech.com':
                return simplekml.Color.darkgoldenrod, 1
            case 'infiniumcm.com':
                return simplekml.Color.darksalmon, 1
            case 'tatora.com':
                return simplekml.Color.darkseagreen, 1
            case 'midwestics.com':
                return simplekml.Color.indigo, 1
            case 'apsaranetworks.com':
                return simplekml.Color.lemonchiffon, 1
            case 'bsonetwork.com':
                return simplekml.Color.khaki, 1
            case 'striketechnologies.com':
                return simplekml.Color.gray, 1
            case 'akingump.com':
                return simplekml.Color.greenyellow, 1
            case 'surveillancetechs.com':
                return simplekml.Color.lightcoral, 1
            case 'bobbroadband.com':
                return simplekml.Color.orange, 1
            case 'gmail.com':
                return simplekml.Color.snow, 1
            case 'newlinenet.com':
                return simplekml.Color.silver, 1
            case 'weblineholdings.com':
                return simplekml.Color.darkmagenta, 1
            case 'highland-systems.com':
                return simplekml.Color.olive, 1
            case 'wacorp.net':
                return simplekml.Color.goldenrod, 1
            case 'scientelsolutions.com':
                return simplekml.Color.gainsboro, 1
            case 'duanemorris.com':
                return simplekml.Color.chocolate, 1
            case 'RADYN.com':
                return simplekml.Color.hotpink, 1
            case 'cellanet.co.ul':
                return simplekml.Color.violet, 1
            case 'teza.com':
                return simplekml.Color.tan, 1
            case default:
                return simplekml.Color.black, 1
'''

def getAddress(entity_name):
    if entity_name != None and "@" in entity_name:
        address = entity_name.split("@", 1)[1]
        match address:
            case "mckay-brothers.com":
                return "mckay-brothers.com"
            case "wcwtech.com":
                return "wcwtech.com"
            case "geodesicnetworks.com":
                return "geodesicnetworks.com"
            case 'auburndata.com':
                return 'auburndata.com'
            case 'abservicesllc.com':
                return 'abservicesllc.com'
            case 'NeXXComwireless.com':
                return 'NeXXComwireless.com'
            case 'isignalnetworks.com':
                return 'isignalnetworks.com'
            case 'anova-tech.com':
                return 'anova-tech.com'
            case 'infiniumcm.com':
                return 'infiniumcm.com'
            case 'tatora.com':
                return 'tatora.com'
            case 'midwestics.com':
                return 'midwestics.com'
            case 'apsaranetworks.com':
                return  'apsaranetworks.com'
            case 'bsonetwork.com':
                return 'bsonetwork.com'
            case 'striketechnologies.com':
                return 'striketechnologies.com'
            case 'akingump.com':
                return  'akingump.com'
            case 'surveillancetechs.com':
                return 'surveillancetechs.com'
            case 'bobbroadband.com':
                return 'bobbroadband.com'
            case 'gmail.com':
                return 'gmail.com'
            case 'newlinenet.com':
                return 'newlinenet.com'
            case default:
                return 'unrecognized'
    return 'unrecognized'

def color_based_on_path_number(path_number, max_paths):
    r,g,b = num_to_rgb(path_number, max_paths)
    return simplekml.Color.rgb(r,g,b)


def populateKML_with_millimetrewave_paths(kml, cursor, folders):
    sql = """
        select
            en.email as email,
            st_x(tlo.lat_long_point) as lon_decimal,
            st_y(tlo.lat_long_point) as lat_decimal,
            tlo.ground_elevation as t_ground_elevation,
            st_x(rlo.lat_long_point) as r_lon_decimal,
            st_y(rlo.lat_long_point) as r_lat_decimal,
            rlo.ground_elevation as r_ground_elevation,
            lh.radio_service_code as status,
            lh.expired_date as expired_date,
            lh.cancellation_date as cancellation_date,
            lh.effective_date as effective_date,
            CONCAT(mp.unique_system_identifier, '_', mp.callsign) as uis
            from 
                jieshu_table as en,
                millimetrewave_paths as mp,
                millimetrewave_location as tlo,
                millimetrewave_location as rlo,
                microwave_license_header as lh
            where 
                en.call_sign = mp.callsign
                and tlo.call_sign = mp.callsign
                and tlo.location_number = mp.transmit_location_number
                and rlo.call_sign = mp.callsign
                and rlo.location_number = mp.receiver_location_number
                and tlo.lat_long_point IS NOT NULL
                and rlo.lat_long_point IS NOT NULL
                and en.call_sign = lh.call_sign 
            limit 10000000
        """
            
    cursor.execute(sql)
    results = cursor.fetchall()


    for result in results:
        lat_dec = result['lat_decimal']
        lon_dec = result['lon_decimal'] # * -1
        
        
        r_lat_dec = result['r_lat_decimal']
        r_lon_dec = result['r_lon_decimal']
        
        t_ground_elevation = 0
        r_ground_elevation = 0
        
        try:
            t_ground_elevation = float(result['t_ground_elevation']) #
            r_ground_elevation = float(result['r_ground_elevation']) # elevation is stored in meters
        except Exception as e:
            print("Exception but ok")
            pass
            
        if entity not in folders:
            entityFolders = []
            fol = kml.newfolder(name=entity)
            entityFolders.append(fol)
            millimetrefol = fol.newfolder(name="millimetre wave")
            entityFolders.append(millimetrefol)
            microwavefol = fol.newfolder(name="microwave")
            entityFolders.append(microwavefol)
            folders[entity] = entityFolders

        fol = folders[entity][1]
        line_string = fol.newlinestring(name=result['uis'], coords=[(lon_dec, lat_dec, t_ground_elevation), (r_lon_dec, r_lat_dec, r_ground_elevation)])
        if result['effective_date'] != None:
            line_string.timespan.begin = toKMLdate(result['effective_date'])
        if result['status'] == 'C' and result['cancellation_date'] != None:
            line_string.timespan.end = toKMLdate(result['cancellation_date'])
        elif result['status'] == 'E' and result['expired_date'] != None:
            line_string.timespan.end = toKMLdate(result['expired_date'])

        color = determineStyle(result['email'])

        line_string.style.linestyle.color = color

    return kml

def populateKML_with_microwave_paths(kml, cursor, folders):
    sql = """
            select
                en.email as email,
                st_x(tlo.lat_long_point) as lon_decimal,
                st_y(tlo.lat_long_point) as lat_decimal,
                tlo.ground_elevation as t_ground_elevation,
                st_x(rlo.lat_long_point) as r_lon_decimal,
                st_y(rlo.lat_long_point) as r_lat_decimal,
                rlo.ground_elevation as r_ground_elevation,
                lh.radio_service_code as status,
                lh.expired_date as expired_date,
                lh.cancellation_date as cancellation_date,
                lh.effective_date as effective_date,
                CONCAT(mp.unique_system_identifier, '_', mp.callsign) as uis
                from 
                    jieshu_table as en,
                    microwave_paths as mp,
                    fcc_location as tlo,
                    fcc_location as rlo,
                    microwave_license_header as lh
                where 
                    en.call_sign = mp.callsign
                    and tlo.call_sign = mp.callsign
                    and tlo.location_number = mp.transmit_location_number
                    and rlo.call_sign = mp.callsign
                    and rlo.location_number = mp.receiver_location_number
                    and tlo.lat_long_point IS NOT NULL
                    and rlo.lat_long_point IS NOT NULL
                    and en.call_sign = lh.call_sign 
                limit 100000
            """

    pre_sql = """
        SELECT count(*) as cunt
        FROM finalTable2 
    """
    cursor.execute(pre_sql)
    results = cursor.fetchall()
    max_paths = 0
    for result in results:
        max_paths = result['cunt']


    sql = """
            select
                ft1.path_number as pn,
                ft1.call_sign as cs,
                t_lat as lat_decimal,
                t_lon as lon_decimal,
                r_lat as r_lat_decimal,
                r_lon as r_lon_decimal,
                r_height as r_height,
                ft1.email as email,
                mlh.radio_service_code as status,
                mlh.expired_date as expired_date,
                mlh.cancellation_date as cancellation_date,
                mlh.effective_date as effective_date
                from 
                    finalTable1 ft1 NATURAL JOIN finalTable2 ft2 JOIN microwave_license_header mlh ON (ft1.call_sign = mlh.call_sign)
        """


    cursor.execute(sql)
    results = cursor.fetchall()

    for result in results:
        lat_dec = result['lat_decimal']
        lon_dec = result['lon_decimal'] # * -1
        
        
        r_lat_dec = result['r_lat_decimal']
        r_lon_dec = result['r_lon_decimal']
        
        t_ground_elevation = 0
        r_ground_elevation = 0

        try:
            t_ground_elevation = float(result['t_ground_elevation']) #
            r_ground_elevation = float(result['r_height']) # elevation is stored in meters
        except Exception as e:
            pass

        entity = getAddress(result['email'])

        if entity not in folders:
            entityFolders = []
            fol = kml.newfolder(name=entity)
            entityFolders.append(fol)
            millimetrefol = fol.newfolder(name="millimetre wave")
            entityFolders.append(millimetrefol)
            microwavefol = fol.newfolder(name="microwave")
            entityFolders.append(microwavefol)
            folders[entity] = entityFolders
        # TODO: CHANGE IDx 
        fol = folders[entity][1]

        line_string = fol.newlinestring(name=result['email'], coords=[(lon_dec, lat_dec, t_ground_elevation), (r_lon_dec, r_lat_dec, r_ground_elevation)])
        
        if result['effective_date'] != None:
            line_string.timespan.begin = toKMLdate(result['effective_date'])
        if result['status'] == 'C' and result['cancellation_date'] != None:
            line_string.timespan.end = toKMLdate(result['cancellation_date'])
        elif result['status'] == 'E' and result['expired_date'] != None:
            line_string.timespan.end = toKMLdate(result['expired_date'])

        color = color_based_on_path_number(result['pn'], max_paths)

        line_string.style.linestyle.color = color
    return kml

def populateKML_with_broker_dealers(kml, cursor):
    sql = """
        select
            company_name as name,
            latitude as latitude,
            longtitude as longitude
            FROM bds_location
        """

    cursor.execute(sql)
    results = cursor.fetchall()
    

    for result in results:
        lat = result['latitude']
        long = result['longitude']


        if lat != None and long != None:
            #if sw_long == None or sw_lat == None or ne_lat == None or ne_long == None:
            point = kml.newpoint()
            point.coords = [(long, lat)]
            #point.style.labelstyle.color = simplekml.Color.red
            #point.style.labelstyle.scale = 1
            #else:
            #    polygon = kml.newpolygon(name=result['name'])
            #    polygon.outerboundaryis.coords= [(ne_long, ne_lat), (sw_long, sw_lat)]
            #    polygon.style.labelstyle.color = simplekml.Color.red
            #    polygon.style.labelstyle.scale = 1



    return kml

def populateKML_with_fcc_addresses(kml, cursor):
    sql = """
        SELECT lat, lng
        FROM (Select max(lat) as lat, max(lon) as lng FROM FCCPoints GROUP BY formatted_address) as unique_addresses
        WHERE EXISTS(SELECT latitude, longtitude
                            FROM bds_location
                            WHERE SQRT(POW(69.1 * (latitude - unique_addresses.lat), 2) + POW(69.1 * (unique_addresses.lng - longtitude) * COS(latitude / 57.3), 2)) < 0.5) 
        LIMIT 10000
        """

    cursor.execute(sql)
    results = cursor.fetchall()
    

    for result in results:
        lat = result['lat']
        long = result['lng']
        '''
        if lat != None and long != None:
            point = kml.newpoint()
            point.coords = [(long, lat)]
            point.style.iconstyle.color = simplekml.Color.blue
        '''

    return kml

try:
    connection = pymysql.connect(host='127.0.0.1',
                                     port=3306,
                                     user='root',
                                     password='vagrant',
                                     db='python_fcc_db_test',
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)
    kml = simplekml.Kml()
    kml.document.name = "data"
    print("Results: <ul>")
    print ("Content-Type: application/vnd.google-earth.kml+xml\n")

    folders = {}
    with connection.cursor() as cursor:
        #kml = populateKML_with_millimetrewave_paths(kml, cursor, folders)
        kml = populateKML_with_microwave_paths(kml, cursor, folders)
        #kml = populateKML_with_broker_dealers(kml, cursor)
        #kml = populateKML_with_fcc_addresses(kml, cursor)
        print(kml.kml())
        
except Exception as error:
    print(error)
finally:
    connection.close()



